﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CISModelGenerator.CWS;
using System.Reflection;
using System.ServiceModel;

namespace CISModelGenerator
{
    public partial class BrowseLL : Form
    {
        private static readonly log4net.ILog log =
          log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //pub variables
        //Create Document Management Client service
        public static DocumentManagementClient docMan = new DocumentManagementClient();

        //Creating OTAuth  object and setting the Authentication token:
        public static OTAuthentication OTAuth = new OTAuthentication();

        private static BrowseLL _instance;
        public static BrowseLL Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new BrowseLL();

                return _instance;
            }
        }
        public BrowseLL()
        {
            InitializeComponent();
        }

        private void BrowseLL_Load(object sender, EventArgs e)
        {
            string EnterPriseWSName = null;
            long EnterPriseWSNodeId = 0;

            OTAuth.AuthenticationToken = General.authToken;
            Node EntrPrseNode = new Node();

            EntrPrseNode = docMan.GetRootNode(ref OTAuth, "EnterpriseWS");
            if (EntrPrseNode != null)
            {
                EnterPriseWSName = EntrPrseNode.Name.ToString();
                EnterPriseWSNodeId = EntrPrseNode.ID;

                //Root node
                this.treeView_cs_list_contents.Nodes.Clear();
                this.treeView_cs_list_contents.SelectedNode = this.treeView_cs_list_contents.Nodes.Add(EnterPriseWSNodeId.ToString(), EnterPriseWSName, 2, 2);
                this.treeView_cs_list_contents.ExpandAll();
            }
        }

        private void treeView_cs_list_contents_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                Console.WriteLine(this.treeView_cs_list_contents.SelectedNode.Name);
                Console.WriteLine(this.treeView_cs_list_contents.SelectedNode.Text);

                //Display objID
                General.parentFolderId = long.Parse(this.treeView_cs_list_contents.SelectedNode.Name);

                ListRootNode(long.Parse(this.treeView_cs_list_contents.SelectedNode.Name));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
            }
        }

        private void ListRootNode(long ParentID)
        {
            Node[] ListofNodes = new Node[] { };
            try
            {
                long selectedstrID = long.Parse(this.treeView_cs_list_contents.SelectedNode.Name);
                string selectedstrName = this.treeView_cs_list_contents.SelectedNode.Text;

                if (this.treeView_cs_list_contents.SelectedNode.Text == selectedstrName)
                {
                    ListofNodes = docMan.ListNodes(ref OTAuth, ParentID, true);

                    if (ListofNodes != null)
                    {
                        this.treeView_cs_list_contents.SelectedNode.Nodes.Clear();

                        foreach (var rootNodes in ListofNodes.OfType<Node>().OrderBy(n => n.Name))
                        {
                            if (rootNodes.Type == "Folder")
                            {
                                this.treeView_cs_list_contents.SelectedNode.Nodes.Add(rootNodes.ID.ToString(), rootNodes.Name, 2, 2);
                            }
                        }
                    }
                }

                this.treeView_cs_list_contents.SelectedNode.ExpandAll();

            }
            catch (FaultException e)
            {

                System.Diagnostics.Debug.WriteLine("{0} : {1}\n", e.Code.Name, e.Message);
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
            }
        }
    }
}
