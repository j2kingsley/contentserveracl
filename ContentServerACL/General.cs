﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using CISModelGenerator.CWS;

namespace CISModelGenerator
{
    public class General
    {
        public static long parentFolderId = 0;

        public static string authToken = null;
        public static string otdsTicket = null;
        public static string cSRestApiUrl = null;

        public static bool validLicense { get; set; }

        public static double gDouble_RefreshInterval;

        //cred:
        public static string username = null;
        public static string password = null;

        public static bool gstrAddGroupsFlag { get; set; }

        public static string gstrExcelDelimiter { get; set; }
        public static string gstrNodeRightTypeDefaultAccess { get; set; }
        public static string gstrNodeRightTypeAssignedAccess { get; set; }
        public static dynamic gstrPermissionMappings { get; set; }

        public static string gstrExcelColumnRange { get; set; }
        public static string gstrFolderColumnIdentifier { get; set; }
        public static string gstrGroupColumnIdentifier { get; set; }
        public static string gstrLevel1 { get; set; }
        public static string gstrLevel2 { get; set; }
        public static string gstrLevel3 { get; set; }
        public static string gstrLevel4 { get; set; }
        public static string gstrLevel5 { get; set; }
        public static string gstrLevel6 { get; set; }
        public static string gstrLevel7 { get; set; }
        public static string gstrLevel8 { get; set; }
        public static string gstrLevel9 { get; set; }
        public static string gstrLevel10 { get; set; }
        
        public static long folderCreatedCount { get; set; }
        public static long folderExistingCount { get; set; }

        public static List<string> listOfPermisisonTypes { get; set; }

        //Export permissions:
        //public static bool exportFlag { get; set; }
        public static int rowCounter { get; set; }
    }
}
