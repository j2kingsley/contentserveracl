﻿namespace CISModelGenerator
{
    partial class ContentServerACL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContentServerACL));
            this.imageList_LivelinkTreeIcons = new System.Windows.Forms.ImageList(this.components);
            this.treeView_cs_list_contents = new System.Windows.Forms.TreeView();
            this.groupBox_groups = new System.Windows.Forms.GroupBox();
            this.treeView_access_list = new System.Windows.Forms.TreeView();
            this.groupBox_groups_permissions = new System.Windows.Forms.GroupBox();
            this.checkBox_Delete = new System.Windows.Forms.CheckBox();
            this.checkBox_DeleteVersions = new System.Windows.Forms.CheckBox();
            this.checkBox_EditPermissions = new System.Windows.Forms.CheckBox();
            this.checkBox_Reserve = new System.Windows.Forms.CheckBox();
            this.checkBox_AddItems = new System.Windows.Forms.CheckBox();
            this.checkBox_EditAttributes = new System.Windows.Forms.CheckBox();
            this.checkBox_Modify = new System.Windows.Forms.CheckBox();
            this.checkBox_SeeContents = new System.Windows.Forms.CheckBox();
            this.checkBox_see = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label_status_update = new System.Windows.Forms.Label();
            this.checkBox_listExceptions = new System.Windows.Forms.CheckBox();
            this.button_browseFolder = new System.Windows.Forms.Button();
            this.textBox_destinationFolder = new System.Windows.Forms.TextBox();
            this.button_cancel = new System.Windows.Forms.Button();
            this.button_export = new System.Windows.Forms.Button();
            this.pictureBox_loading = new System.Windows.Forms.PictureBox();
            this.label_export_status = new System.Windows.Forms.Label();
            this.bgWorker_permissionExporter = new System.ComponentModel.BackgroundWorker();
            this.folderBrowserDialog_csv = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox_groups.SuspendLayout();
            this.groupBox_groups_permissions.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_loading)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList_LivelinkTreeIcons
            // 
            this.imageList_LivelinkTreeIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_LivelinkTreeIcons.ImageStream")));
            this.imageList_LivelinkTreeIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList_LivelinkTreeIcons.Images.SetKeyName(0, "folder16.gif");
            this.imageList_LivelinkTreeIcons.Images.SetKeyName(1, "folder16_.png");
            this.imageList_LivelinkTreeIcons.Images.SetKeyName(2, "mime_folder.png");
            this.imageList_LivelinkTreeIcons.Images.SetKeyName(3, "12x_owner.gif");
            this.imageList_LivelinkTreeIcons.Images.SetKeyName(4, "guy.gif");
            this.imageList_LivelinkTreeIcons.Images.SetKeyName(5, "2-guys.gif");
            this.imageList_LivelinkTreeIcons.Images.SetKeyName(6, "publicaccess.gif");
            this.imageList_LivelinkTreeIcons.Images.SetKeyName(7, "ownergroup.gif");
            this.imageList_LivelinkTreeIcons.Images.SetKeyName(8, "16xdomaingrp.gif");
            this.imageList_LivelinkTreeIcons.Images.SetKeyName(9, "");
            this.imageList_LivelinkTreeIcons.Images.SetKeyName(10, "");
            this.imageList_LivelinkTreeIcons.Images.SetKeyName(11, "");
            // 
            // treeView_cs_list_contents
            // 
            this.treeView_cs_list_contents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView_cs_list_contents.HideSelection = false;
            this.treeView_cs_list_contents.ImageIndex = 2;
            this.treeView_cs_list_contents.ImageList = this.imageList_LivelinkTreeIcons;
            this.treeView_cs_list_contents.Location = new System.Drawing.Point(3, 3);
            this.treeView_cs_list_contents.Name = "treeView_cs_list_contents";
            this.treeView_cs_list_contents.SelectedImageIndex = 0;
            this.treeView_cs_list_contents.Size = new System.Drawing.Size(703, 877);
            this.treeView_cs_list_contents.TabIndex = 0;
            this.treeView_cs_list_contents.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_cs_list_contents_AfterSelect);
            // 
            // groupBox_groups
            // 
            this.groupBox_groups.Controls.Add(this.treeView_access_list);
            this.groupBox_groups.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_groups.Location = new System.Drawing.Point(712, 3);
            this.groupBox_groups.Name = "groupBox_groups";
            this.groupBox_groups.Size = new System.Drawing.Size(703, 877);
            this.groupBox_groups.TabIndex = 2;
            this.groupBox_groups.TabStop = false;
            this.groupBox_groups.Text = "Groups";
            // 
            // treeView_access_list
            // 
            this.treeView_access_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView_access_list.ImageIndex = 0;
            this.treeView_access_list.ImageList = this.imageList_LivelinkTreeIcons;
            this.treeView_access_list.Location = new System.Drawing.Point(3, 34);
            this.treeView_access_list.Name = "treeView_access_list";
            this.treeView_access_list.SelectedImageIndex = 0;
            this.treeView_access_list.Size = new System.Drawing.Size(697, 840);
            this.treeView_access_list.TabIndex = 9;
            this.treeView_access_list.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_access_list_AfterSelect);
            // 
            // groupBox_groups_permissions
            // 
            this.groupBox_groups_permissions.Controls.Add(this.checkBox_Delete);
            this.groupBox_groups_permissions.Controls.Add(this.checkBox_DeleteVersions);
            this.groupBox_groups_permissions.Controls.Add(this.checkBox_EditPermissions);
            this.groupBox_groups_permissions.Controls.Add(this.checkBox_Reserve);
            this.groupBox_groups_permissions.Controls.Add(this.checkBox_AddItems);
            this.groupBox_groups_permissions.Controls.Add(this.checkBox_EditAttributes);
            this.groupBox_groups_permissions.Controls.Add(this.checkBox_Modify);
            this.groupBox_groups_permissions.Controls.Add(this.checkBox_SeeContents);
            this.groupBox_groups_permissions.Controls.Add(this.checkBox_see);
            this.groupBox_groups_permissions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_groups_permissions.Location = new System.Drawing.Point(1421, 3);
            this.groupBox_groups_permissions.Name = "groupBox_groups_permissions";
            this.groupBox_groups_permissions.Size = new System.Drawing.Size(704, 877);
            this.groupBox_groups_permissions.TabIndex = 3;
            this.groupBox_groups_permissions.TabStop = false;
            this.groupBox_groups_permissions.Text = "Group Permissions";
            // 
            // checkBox_Delete
            // 
            this.checkBox_Delete.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_Delete.AutoSize = true;
            this.checkBox_Delete.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox_Delete.Location = new System.Drawing.Point(109, 566);
            this.checkBox_Delete.Name = "checkBox_Delete";
            this.checkBox_Delete.Size = new System.Drawing.Size(131, 36);
            this.checkBox_Delete.TabIndex = 8;
            this.checkBox_Delete.Text = "Delete";
            this.checkBox_Delete.UseVisualStyleBackColor = true;
            // 
            // checkBox_DeleteVersions
            // 
            this.checkBox_DeleteVersions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_DeleteVersions.AutoSize = true;
            this.checkBox_DeleteVersions.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox_DeleteVersions.Location = new System.Drawing.Point(66, 500);
            this.checkBox_DeleteVersions.Name = "checkBox_DeleteVersions";
            this.checkBox_DeleteVersions.Size = new System.Drawing.Size(249, 36);
            this.checkBox_DeleteVersions.TabIndex = 7;
            this.checkBox_DeleteVersions.Text = "Delete Versions";
            this.checkBox_DeleteVersions.UseVisualStyleBackColor = true;
            // 
            // checkBox_EditPermissions
            // 
            this.checkBox_EditPermissions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_EditPermissions.AutoSize = true;
            this.checkBox_EditPermissions.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox_EditPermissions.Location = new System.Drawing.Point(137, 626);
            this.checkBox_EditPermissions.Name = "checkBox_EditPermissions";
            this.checkBox_EditPermissions.Size = new System.Drawing.Size(260, 36);
            this.checkBox_EditPermissions.TabIndex = 6;
            this.checkBox_EditPermissions.Text = "Edit Permissions";
            this.checkBox_EditPermissions.UseVisualStyleBackColor = true;
            // 
            // checkBox_Reserve
            // 
            this.checkBox_Reserve.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_Reserve.AutoSize = true;
            this.checkBox_Reserve.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox_Reserve.Location = new System.Drawing.Point(66, 434);
            this.checkBox_Reserve.Name = "checkBox_Reserve";
            this.checkBox_Reserve.Size = new System.Drawing.Size(153, 36);
            this.checkBox_Reserve.TabIndex = 5;
            this.checkBox_Reserve.Text = "Reserve";
            this.checkBox_Reserve.UseVisualStyleBackColor = true;
            // 
            // checkBox_AddItems
            // 
            this.checkBox_AddItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_AddItems.AutoSize = true;
            this.checkBox_AddItems.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox_AddItems.Location = new System.Drawing.Point(66, 368);
            this.checkBox_AddItems.Name = "checkBox_AddItems";
            this.checkBox_AddItems.Size = new System.Drawing.Size(174, 36);
            this.checkBox_AddItems.TabIndex = 4;
            this.checkBox_AddItems.Text = "Add Items";
            this.checkBox_AddItems.UseVisualStyleBackColor = true;
            // 
            // checkBox_EditAttributes
            // 
            this.checkBox_EditAttributes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_EditAttributes.AutoSize = true;
            this.checkBox_EditAttributes.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox_EditAttributes.Location = new System.Drawing.Point(66, 299);
            this.checkBox_EditAttributes.Name = "checkBox_EditAttributes";
            this.checkBox_EditAttributes.Size = new System.Drawing.Size(226, 36);
            this.checkBox_EditAttributes.TabIndex = 3;
            this.checkBox_EditAttributes.Text = "Edit Attributes";
            this.checkBox_EditAttributes.UseVisualStyleBackColor = true;
            // 
            // checkBox_Modify
            // 
            this.checkBox_Modify.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_Modify.AutoSize = true;
            this.checkBox_Modify.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox_Modify.Location = new System.Drawing.Point(38, 232);
            this.checkBox_Modify.Name = "checkBox_Modify";
            this.checkBox_Modify.Size = new System.Drawing.Size(132, 36);
            this.checkBox_Modify.TabIndex = 2;
            this.checkBox_Modify.Text = "Modify";
            this.checkBox_Modify.UseVisualStyleBackColor = true;
            // 
            // checkBox_SeeContents
            // 
            this.checkBox_SeeContents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_SeeContents.AutoSize = true;
            this.checkBox_SeeContents.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox_SeeContents.Location = new System.Drawing.Point(38, 166);
            this.checkBox_SeeContents.Name = "checkBox_SeeContents";
            this.checkBox_SeeContents.Size = new System.Drawing.Size(220, 36);
            this.checkBox_SeeContents.TabIndex = 1;
            this.checkBox_SeeContents.Text = "See Contents";
            this.checkBox_SeeContents.UseVisualStyleBackColor = true;
            // 
            // checkBox_see
            // 
            this.checkBox_see.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_see.AutoSize = true;
            this.checkBox_see.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox_see.Location = new System.Drawing.Point(19, 104);
            this.checkBox_see.Name = "checkBox_see";
            this.checkBox_see.Size = new System.Drawing.Size(99, 36);
            this.checkBox_see.TabIndex = 0;
            this.checkBox_see.Text = "See";
            this.checkBox_see.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33332F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.Controls.Add(this.label_status_update, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.checkBox_listExceptions, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.groupBox_groups, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox_groups_permissions, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.treeView_cs_list_contents, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.button_browseFolder, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBox_destinationFolder, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.button_cancel, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.button_export, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox_loading, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label_export_status, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(2128, 1262);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // label_status_update
            // 
            this.label_status_update.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_status_update.AutoSize = true;
            this.label_status_update.Location = new System.Drawing.Point(3, 1009);
            this.label_status_update.Name = "label_status_update";
            this.label_status_update.Size = new System.Drawing.Size(703, 126);
            this.label_status_update.TabIndex = 13;
            this.label_status_update.Text = ".";
            // 
            // checkBox_listExceptions
            // 
            this.checkBox_listExceptions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_listExceptions.AutoSize = true;
            this.checkBox_listExceptions.Location = new System.Drawing.Point(712, 1012);
            this.checkBox_listExceptions.Name = "checkBox_listExceptions";
            this.checkBox_listExceptions.Size = new System.Drawing.Size(703, 120);
            this.checkBox_listExceptions.TabIndex = 10;
            this.checkBox_listExceptions.Text = "List only exceptions";
            this.checkBox_listExceptions.UseVisualStyleBackColor = true;
            // 
            // button_browseFolder
            // 
            this.button_browseFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_browseFolder.Location = new System.Drawing.Point(1421, 886);
            this.button_browseFolder.Name = "button_browseFolder";
            this.button_browseFolder.Size = new System.Drawing.Size(704, 120);
            this.button_browseFolder.TabIndex = 7;
            this.button_browseFolder.Text = "Browse";
            this.button_browseFolder.UseVisualStyleBackColor = true;
            this.button_browseFolder.Click += new System.EventHandler(this.button_browseFolder_Click);
            // 
            // textBox_destinationFolder
            // 
            this.textBox_destinationFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_destinationFolder.Location = new System.Drawing.Point(712, 886);
            this.textBox_destinationFolder.Name = "textBox_destinationFolder";
            this.textBox_destinationFolder.Size = new System.Drawing.Size(703, 38);
            this.textBox_destinationFolder.TabIndex = 8;
            // 
            // button_cancel
            // 
            this.button_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_cancel.Location = new System.Drawing.Point(712, 1138);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(703, 121);
            this.button_cancel.TabIndex = 6;
            this.button_cancel.Text = "Cancel";
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // button_export
            // 
            this.button_export.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_export.Location = new System.Drawing.Point(1421, 1138);
            this.button_export.Name = "button_export";
            this.button_export.Size = new System.Drawing.Size(704, 121);
            this.button_export.TabIndex = 4;
            this.button_export.Text = "Export";
            this.button_export.UseVisualStyleBackColor = true;
            this.button_export.Click += new System.EventHandler(this.button_export_Click);
            // 
            // pictureBox_loading
            // 
            this.pictureBox_loading.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pictureBox_loading.Enabled = false;
            this.pictureBox_loading.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_loading.Image")));
            this.pictureBox_loading.Location = new System.Drawing.Point(3, 1164);
            this.pictureBox_loading.Name = "pictureBox_loading";
            this.pictureBox_loading.Size = new System.Drawing.Size(163, 68);
            this.pictureBox_loading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_loading.TabIndex = 11;
            this.pictureBox_loading.TabStop = false;
            this.pictureBox_loading.Visible = false;
            // 
            // label_export_status
            // 
            this.label_export_status.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_export_status.AutoSize = true;
            this.label_export_status.Location = new System.Drawing.Point(3, 883);
            this.label_export_status.Name = "label_export_status";
            this.label_export_status.Size = new System.Drawing.Size(703, 126);
            this.label_export_status.TabIndex = 12;
            this.label_export_status.Text = "Status: ";
            // 
            // bgWorker_permissionExporter
            // 
            this.bgWorker_permissionExporter.WorkerReportsProgress = true;
            this.bgWorker_permissionExporter.WorkerSupportsCancellation = true;
            this.bgWorker_permissionExporter.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorker_permissionExporter_DoWork);
            this.bgWorker_permissionExporter.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWorker_permissionExporter_ProgressChanged);
            this.bgWorker_permissionExporter.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorker_permissionExporter_RunWorkerCompleted);
            // 
            // ContentServerACL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2128, 1262);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(2160, 1350);
            this.Name = "ContentServerACL";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Content Server - ACL";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ContentServerACL_FormClosing);
            this.Load += new System.EventHandler(this.DisplayContentServerFolders_Load_1);
            this.groupBox_groups.ResumeLayout(false);
            this.groupBox_groups_permissions.ResumeLayout(false);
            this.groupBox_groups_permissions.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_loading)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        //private System.Windows.Forms.TreeView treeView1;
        //private System.Windows.Forms.Label label_status;
        private System.Windows.Forms.ImageList imageList_LivelinkTreeIcons;
        private System.Windows.Forms.TreeView treeView_cs_list_contents;
        private System.Windows.Forms.GroupBox groupBox_groups;
        private System.Windows.Forms.TreeView treeView_access_list;
        private System.Windows.Forms.GroupBox groupBox_groups_permissions;
        private System.Windows.Forms.CheckBox checkBox_Delete;
        private System.Windows.Forms.CheckBox checkBox_DeleteVersions;
        private System.Windows.Forms.CheckBox checkBox_EditPermissions;
        private System.Windows.Forms.CheckBox checkBox_Reserve;
        private System.Windows.Forms.CheckBox checkBox_AddItems;
        private System.Windows.Forms.CheckBox checkBox_EditAttributes;
        private System.Windows.Forms.CheckBox checkBox_Modify;
        private System.Windows.Forms.CheckBox checkBox_SeeContents;
        private System.Windows.Forms.CheckBox checkBox_see;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button_export;
        private System.ComponentModel.BackgroundWorker bgWorker_permissionExporter;
        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.Button button_browseFolder;
        private System.Windows.Forms.TextBox textBox_destinationFolder;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog_csv;
        private System.Windows.Forms.CheckBox checkBox_listExceptions;
        private System.Windows.Forms.PictureBox pictureBox_loading;
        private System.Windows.Forms.Label label_export_status;
        private System.Windows.Forms.Label label_status_update;
    }
}