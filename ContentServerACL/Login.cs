﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CISModelGenerator.CWS;
using CISModelGenerator;

namespace CISModelGenerator
{
    public partial class Login : Form
    {
        private static readonly log4net.ILog log =
          log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        public Login()
        {
            InitializeComponent();
            LoadSettings();
        }

        private void button_login_Load(object sender, EventArgs e)
        {
            //Creating Log folder, incase if doesn't exist
            Utils.CreateLogFolder();

            //Saving Finger Print Details in App.config
            Utils.ReadAllSettings();

            //Validate Licensing
            Utils.ValidateLicense();
        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
                           
            this.Close();
        }

        private void btn_Login_Click(object sender, EventArgs e)
        {
            if (this.textBox_cws_url.Text == "")
            {
                this.Refresh();
                this.label_status.Text = "Please Enter URI";
                this.Refresh();
            }
            else if (this.textBox_username.Text == "")
            {
                this.Refresh();
                this.label_status.Text = "Please Enter UserName";
                this.Refresh();
            }
            else
            {
                if (this.textBox_password.Text == "")
                {
                    this.Refresh();
                    this.label_status.Text = "Please Enter Password";
                    this.Refresh();
                }
                else
                {
                    SingleLoginFunc();
                    //License part
                    //if (General.validLicense)
                    //{
                    //   // SingleLoginFunc();
                    //}
                    //else
                    //{
                    //    MessageBox.Show("Unlicensed Version: Please Contact Tenzing for License Key!", "App is not licensed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    //}
                }
            }
        }

        /// <summary>
        /// Load the user login settings.
        /// </summary>
        private void LoadSettings()
        {
            try
            {
                Properties.Settings props = Properties.Settings.Default;

                this.textBox_cws_url.Text = props.URL;

                this.textBox_username.Text = props.UserName;
                this.textBox_password.Text = Utils.Decrypt(props.Password);

            }
            catch (Exception ex)
            {              
                Console.WriteLine(ex.Message);

                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
            }

        }
        //Single Loginf Function
        private void SingleLoginFunc()
        {
            //Save the Settings
            SaveSettings();   //Enhancement :Saving after successfull login as well

            CWS();
        }
        /// <summary>
        /// Save the settings.
        /// </summary>
        private void SaveSettings()
        {
            try
            {
                Properties.Settings props = Properties.Settings.Default;

                props.URL = this.textBox_cws_url.Text;
                props.UserName = this.textBox_username.Text;
                props.Password = Utils.Encrypt(this.textBox_password.Text);

                props.Save();

                /// Save the changes in App Config file as well
                ConfigSettings.SaveEndpointAddress(this.textBox_cws_url.Text.ToString().Trim());
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);

                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
            }

        }
        //Using services
        private void CWS()
        {
           
            //The user Credentials:

            General.username = this.textBox_username.Text;
            General.password = this.textBox_password.Text;

            AuthenticationClient AuthClient = null;
            try
            {
                //Create the Authentication Service Client:
                AuthClient = new AuthenticationClient();
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                this.label_status.Text = ex.Message + "\n";
                this.Refresh();
                return;
            }


            //Storing Authentication Token:
            //string AuthToken = null;      //Made it Public to use outside

            try
            {

                this.Refresh();
                Console.Write("Authenticating User ...");
                this.label_status.Text = "Authenticating User ..";
                this.Refresh();

                //General.AuthToken = AuthClient.AuthenticateUser(General.username, General.password);

                try
                {
                    General.authToken = AuthClient.AuthenticateUser(General.username, General.password);

                    System.Diagnostics.Debug.WriteLine("Success!\n");
                    this.label_status.Text = "Success!\n";
                    this.Refresh();                          
           
                    log.Info(DateTime.Now + " | " + "Authorized");
                    log.Info(DateTime.Now + " | " + "Logged in by : " + General.username);
                    log.Info("------------------------------------------------------------------------");
                    
                    //Enhancement: Authentication Timeout error
                    Utils.CallTimerEvent();

                    //Enhancement
                    //Save these login credentials again if the details are correct
                    SaveSettings();

                    //Call Folder Creator Form
                    //CISModelGeneratorHome cISModelGeneratorHome = new CISModelGeneratorHome();
                    //cISModelGeneratorHome.Show();

                    //Call Export Window
                    ContentServerACL contentServerACL = new ContentServerACL();
                    contentServerACL.Show();


                    this.Hide();
                }
                //catch (EndpointNotFoundException ex)
                catch (Exception ex) when (ex is EndpointNotFoundException || ex is ArgumentException)
                {
                    Console.WriteLine(ex.Message);

                    log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                    log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                    System.Diagnostics.Debug.WriteLine("Failed!");
                    System.Diagnostics.Debug.WriteLine("{0} : {1}\n", ex.StackTrace, ex.Message);
                    this.label_status.Text = "Failed!";

                    this.label_status.Text = ex.Message + "\n";
                    this.Refresh();

                    return;
                }

            }
            catch (FaultException ex)
            {

                Console.WriteLine(ex.Message);

                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                System.Diagnostics.Debug.WriteLine("Failed!");
                System.Diagnostics.Debug.WriteLine("{0} : {1}\n", ex.Code.Name, ex.Message);
                this.label_status.Text = "Failed!";

                this.label_status.Text = ex.Code.Name + " | " + ex.Message + "\n";
                this.Refresh();
                return;

            }
            finally
            {
                // Always close the client
                AuthClient.Close();
                System.Diagnostics.Debug.WriteLine("Client Closed!");
                //this.label_status.Text = "Client Closed!";
                this.Refresh();
            }


        }

        private void textBox_cws_url_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)13)
                {
                    SingleLoginFunc();
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error : " + ex.Message);
            }
        }

        private void textBox_username_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)13)
                {
                    SingleLoginFunc();
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error : " + ex.Message);
            }

        }

        private void textBox_password_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)13)
                {
                    SingleLoginFunc();
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error : " + ex.Message);
            }
        }
    }
}
