﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CISModelGenerator
{
   public class CSVManipulation
    {
        private static readonly log4net.ILog log =
       log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //private Dictionary<Tuple<int, int>, string> _data;
        //Instantiating at the time of declaration to avoid some issues
        private Dictionary<Tuple<int, int>, string> _data = new Dictionary<Tuple<int, int>, string>();
        private int _rows;
        private int _cols;

        public int Rows { get { return _rows; } }
        public int Cols { get { return _cols; } }

        public void CSV()
        {
            Clear();
        }

        public void Clear()
        {
            _rows = 0;
            _cols = 0;
            _data = new Dictionary<Tuple<int, int>, string>();
        }

        public void Open(StreamReader stream, char delim = ',')
        {
            string line;
            int col = 0;
            int row = 0;

            try
            {
                Clear();

                while ((line = stream.ReadLine()) != null)
                {
                    if (line.Length > 0)
                    {
                        string[] values = line.Split(delim);
                        col = 0;
                        foreach (var value in values)
                        {
                            this[col, row] = value;
                            col++;
                        }
                        row++;
                    }
                }
                stream.Close();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

            }
        }

        public void Save(StreamWriter stream, char delim = ',')
        {
            try
            {
                for (int row = 0; row < _rows; row++)
                {
                    for (int col = 0; col < _cols; col++)
                    {
                        stream.Write(this[col, row]);
                        //System.Diagnostics.Debug.WriteLine("Each Cell Value : " + (this[col, row]));
                        if (col < _cols - 1)
                        {
                            stream.Write(delim);
                        }
                    }
                    stream.WriteLine();
                }
                stream.Flush();
                stream.Close();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

            }
        }

        public string this[int col, int row]
        {
            get
            {
                try
                {
                    return _data[new Tuple<int, int>(col, row)];

                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    return "";
                }
            }

            set
            {

                ////Bug#4 :First row Object ID is showing as null or takes last attribute value of last category on given object only if folder is not upgraded but file has been upgraded. That means file has more attribute value
                ////Bug fixed by using following condition
                //if (_data.ContainsKey(new Tuple<int, int>(col, row)))
                //{
                //    System.Diagnostics.Debug.WriteLine("Existing Value So Skip : " + value.ToString().Trim());
                //}
                //else
                //{
                //    _data[new Tuple<int, int>(col, row)] = value.ToString().Trim();
                //    _rows = Math.Max(_rows, row + 1);
                //    _cols = Math.Max(_cols, col + 1);

                //}

                _data[new Tuple<int, int>(col, row)] = value.ToString().Trim();
                _rows = Math.Max(_rows, row + 1);
                _cols = Math.Max(_cols, col + 1);

            }

        }

        //Testing to get column no
        //It wont work for row. bz column name is fixed
        public void GetColNumber(string OnlyColumnValue)
        {
            int Key;



            try
            {
                if (_data.ContainsValue(OnlyColumnValue))
                {
                    //Key    =   _data[
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

            }

        }


    }
}
