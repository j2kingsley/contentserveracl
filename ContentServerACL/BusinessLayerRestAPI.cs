﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net;

namespace CISModelGenerator
{
    public class BusinessLayerRestAPI
    {
        
        private static readonly log4net.ILog log =
       log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public class OtdsTickeT
        {
            public string ticket { get; set; }
        }
        public class GroupsAndPermissions
        {
            public string permissions { get; set; }
        }
        private async Task<bool> UrlIsReachable(string url)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(url);
                    return response.StatusCode == HttpStatusCode.OK;
                }
            }
            catch
            {
                return false;
            }
        }
        //CheckURLValid
        public bool CheckURLValid(string strURL)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(strURL);
            request.AllowAutoRedirect = false; // find out if this site is up and don't follow a redirector
            request.Method = "HEAD";
            try
            {
                using (var response = request.GetResponse())
                {
                    return true;
                }
            }
            catch (WebException  ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                return false;
            }

        }
        public string authenticateUserRest(string username, string password)
        {
            string returnOtdsTicket = null;

            try
            {
                var client = new RestClient(General.cSRestApiUrl);

                var request = new RestRequest("api/{version}/{resourceName}", Method.POST);
                request.AddParameter("username", username);
                request.AddParameter("password", password);
                request.AddUrlSegment("version", "v1");
                request.AddUrlSegment("resourceName", "auth");

                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddHeader("Accept", "application/x-www-form-urlencoded");


                // execute the request
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                var otdsTickeT = JsonConvert.DeserializeObject<OtdsTickeT>(content);
                returnOtdsTicket = otdsTickeT.ticket;

                return returnOtdsTicket;

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);

                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);


                return returnOtdsTicket;
            }
        }

        public bool isSessionActive(string otdsTicket)
        {
            bool returnSessionStatus = false;
            IRestResponse response = new RestResponse();
            try
            {
                var client = new RestClient(General.cSRestApiUrl);

                var request = new RestRequest("api/{version}/{resourceName}", Method.GET);

                request.AddUrlSegment("version", "v1");
                request.AddUrlSegment("resourceName", "serverinfo");

                request.AddHeader("OTCSTicket", otdsTicket);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddHeader("Accept", "application/x-www-form-urlencoded");

                // execute the request
                response = client.Execute(request);
                var content = response.Content;

                dynamic sessionStatus = JsonConvert.DeserializeObject(content);

                if (sessionStatus.error !=  "Session Expired")
                {
                    returnSessionStatus = true;
                }
              
                return returnSessionStatus;

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);

                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);


                return returnSessionStatus;
            }
        }

        public dynamic getNodePermission(string otdsTicket, long nodeId, string resourceNameWithRightID)
        {
            dynamic groupsPermissions = null;
            IRestResponse response = new RestResponse();
            try
            {
                var client = new RestClient(General.cSRestApiUrl);

                //var request = new RestRequest("api/{version}/nodes/{nodeId}/{resourceName}", Method.GET);
                var request = new RestRequest("api/{version}/nodes/{nodeId}/{resourceName}", Method.GET);

                request.AddUrlSegment("version", "v2");
                request.AddUrlSegment("nodeId", nodeId);
                request.AddUrlSegment("resourceName", resourceNameWithRightID);
                //request.AddUrlSegment("rightId", rightId);


                request.AddHeader("OTCSTicket", otdsTicket);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddHeader("Accept", "application/x-www-form-urlencoded");
                
                // execute the request
                response = client.Execute(request);
                var content = response.Content;

                groupsPermissions = JsonConvert.DeserializeObject(content);
                
                return groupsPermissions;

            }
            catch (Exception ex)
            {
                response.ResponseStatus = ResponseStatus.Error;
                response.ErrorMessage = ex.Message;
                response.ErrorException = ex;

                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                return groupsPermissions;
            }
        }

        public dynamic getNodePermissionsList(string otdsTicket, long nodeId)
        {
            dynamic groupsPermissionsList = null;
            IRestResponse response = new RestResponse();
            try
            {
                var client = new RestClient(General.cSRestApiUrl);

                //var request = new RestRequest("api/{version}/nodes/{nodeId}/{resourceName}", Method.GET);
                var request = new RestRequest("api/{version}/nodes/{nodeId}?fields=properties%7Bcontainer%2C+name%2C+type%7D&fields=permissions%7Bright_id%2C+permissions%2C+type%7D&fields=versions%7Bversion_id%7D&expand=permissions%7Bright_id%7D", Method.GET);

                request.AddUrlSegment("version", "v2");
                request.AddUrlSegment("nodeId", nodeId);
                request.AddUrlSegment("resourceName", "permissions");

                request.AddHeader("OTCSTicket", otdsTicket);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddHeader("Accept", "application/x-www-form-urlencoded");

                // execute the request
                response = client.Execute(request);
                var content = response.Content;

                groupsPermissionsList = JsonConvert.DeserializeObject(content);

                return groupsPermissionsList;

            }
            catch (Exception ex)
            {
                response.ResponseStatus = ResponseStatus.Error;
                response.ErrorMessage = ex.Message;
                response.ErrorException = ex;

                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                return groupsPermissionsList;
            }
        }
        public string addNodePermissions(string otdsTicket, long nodeId, long rightId, string permssionList)
        {
            string returnOtdsTicket = null;

            try
            {
                var client = new RestClient(General.cSRestApiUrl);

                var request = new RestRequest("api/{version}/nodes/{nodeId}/permissions/{resourceName}", Method.POST);
                request.AddUrlSegment("version", "v2");
                request.AddUrlSegment("nodeId", nodeId);
                request.AddUrlSegment("resourceName", "custom");

                request.AddHeader("OTCSTicket", otdsTicket);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddHeader("Accept", "application/x-www-form-urlencoded");

                request.AddParameter("right_id", rightId);
                request.AddParameter("permissions", permssionList);

                // execute the request
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                //var otdsTickeT = JsonConvert.DeserializeObject<OtdsTickeT>(content);
                //returnOtdsTicket = otdsTickeT.ticket;

                //return returnOtdsTicket;


                return returnOtdsTicket;

            }
            catch (Exception)
            {

                throw;

                return returnOtdsTicket;
            }
        }


    }
}
