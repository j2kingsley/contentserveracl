﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CISModelGenerator.CWS;

namespace CISModelGenerator
{
    public partial class ContentServerACL : Form
    {
        private static readonly log4net.ILog log =
        log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public StringBuilder AttrValSB = new StringBuilder();
        private static string csvFilePath { get; set; }
        private static bool exportFlag { get; set; }
        private static bool exportSuccessFlag { get; set; }
        private static bool cancelFlag { get; set; }


        private Dictionary<string, int> ColumnNameWithNo = new Dictionary<string, int>();

        private StringBuilder permissionsValue = new StringBuilder();

        //Creating OTAuth  object and setting the Authentication token:
        public static OTAuthentication OTAuth = new OTAuthentication();

        private BusinessLayerRestAPI businessLayerRestAPI = new BusinessLayerRestAPI();

        private static ContentServerACL _instance;
        public static ContentServerACL Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ContentServerACL();

                return _instance;
            }
        }

        public ContentServerACL()
        {
            InitializeComponent();
        }

        private static long gEnterPriseWSNodeId = 0;
        private void DisplayContentServerFolders_Load_1(object sender, EventArgs e)
        {
            DocumentManagementClient docMan = new DocumentManagementClient();
            try
            {
                string EnterPriseWSName = null;
                long EnterPriseWSNodeId = 0;

                OTAuth.AuthenticationToken = General.authToken;

                var isUrlReachable = businessLayerRestAPI.CheckURLValid(General.cSRestApiUrl);
                if (!isUrlReachable)
                {
                    MessageBox.Show("Please enter valid CS Rest API URL!", "Invalid URL", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                //Generate otds Ticket
                General.otdsTicket = businessLayerRestAPI.authenticateUserRest(General.username, General.password);

                Node EntrPrseNode = new Node();

                EntrPrseNode = docMan.GetRootNode(ref OTAuth, "EnterpriseWS");
                if (EntrPrseNode != null)
                {
                    EnterPriseWSName = EntrPrseNode.Name.ToString();
                    EnterPriseWSNodeId = EntrPrseNode.ID;
                    gEnterPriseWSNodeId = EntrPrseNode.ID;
                    //Root node
                    this.treeView_cs_list_contents.Nodes.Clear();
                    this.treeView_cs_list_contents.SelectedNode = this.treeView_cs_list_contents.Nodes.Add(EnterPriseWSNodeId.ToString(), EnterPriseWSName, 0, 0);
                    this.treeView_cs_list_contents.ExpandAll();

                    //ListRootNode(EnterPriseWSNodeId);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
            }
            finally
            {
                docMan.Close();
            }
        }

        private void ListRootNode(long ParentID)
        {
            General.parentFolderId = ParentID;

            DocumentManagementClient docMan = new DocumentManagementClient();

            Node[] ListofNodes = new Node[] { };
            try
            {
                long selectedstrID = long.Parse(this.treeView_cs_list_contents.SelectedNode.Name);
                string selectedstrName = this.treeView_cs_list_contents.SelectedNode.Text;

                if (this.treeView_cs_list_contents.SelectedNode.Text == selectedstrName)
                {
                    ListofNodes = docMan.ListNodes(ref OTAuth, ParentID, true);

                    if (ListofNodes != null)
                    {
                        this.treeView_cs_list_contents.SelectedNode.Nodes.Clear();

                        foreach (var rootNodes in ListofNodes.OfType<Node>().OrderBy(n => n.Name))
                        {
                            if (rootNodes.Type == "Folder")
                            {
                                this.treeView_cs_list_contents.SelectedNode.Nodes.Add(rootNodes.ID.ToString(), rootNodes.Name, 0, 0);
                            }
                            else
                            {
                                this.treeView_cs_list_contents.SelectedNode.Nodes.Add(rootNodes.ID.ToString(), rootNodes.Name, 9, 9);

                            }
                        }
                    }
                }

                this.treeView_cs_list_contents.SelectedNode.ExpandAll();

            }
            catch (FaultException e)
            {
                System.Diagnostics.Debug.WriteLine("{0} : {1}\n", e.Code.Name, e.Message);
            }
            finally
            {
                docMan.Close();
            }
        }

        private void treeView_cs_list_contents_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                Console.WriteLine(this.treeView_cs_list_contents.SelectedNode.Name);
                Console.WriteLine(this.treeView_cs_list_contents.SelectedNode.Text);


                //List nodes
                ListRootNode(long.Parse(this.treeView_cs_list_contents.SelectedNode.Name));

                //Display group and permissions
                ListGroupsPermissions(long.Parse(this.treeView_cs_list_contents.SelectedNode.Name));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
            }
        }

        //List Groups
        private void ListGroupsPermissions(long nodeId)
        {
            dynamic groupsPermissionsList = null;
            string defaultAccess = "Default Access";
            string assignedAccess = "Assigned Access";
            try
            {
                if (General.otdsTicket == null)
                {
                    General.otdsTicket = businessLayerRestAPI.authenticateUserRest(General.username, General.password);
                }
                else if (!businessLayerRestAPI.isSessionActive(General.otdsTicket))
                {
                    General.otdsTicket = businessLayerRestAPI.authenticateUserRest(General.username, General.password);
                }

                groupsPermissionsList = businessLayerRestAPI.getNodePermissionsList(General.otdsTicket, nodeId);

                this.treeView_access_list.Nodes.Clear();
                ClearAllPermissionCheckBoxes();

                //Create 2 root nodes:
                this.treeView_access_list.SelectedNode = this.treeView_access_list.Nodes.Add(defaultAccess, defaultAccess, 7, 7);
                this.treeView_access_list.SelectedNode = this.treeView_access_list.Nodes.Add(assignedAccess, assignedAccess, 8, 8);

                foreach (var eachNodeRights in groupsPermissionsList.results.data.permissions)
                {
                    string groupName = null;
                    string groupRightId = null;
                    if (eachNodeRights.type != "public")
                    {
                        if (eachNodeRights.right_id_expand != null)
                        {
                            if (eachNodeRights.right_id_expand.name != null)
                            {
                                groupName = eachNodeRights.right_id_expand.name;
                                groupRightId = nodeId + ":" + eachNodeRights.right_id_expand.id + ":" + eachNodeRights.type;

                            }
                        }

                    }
                    else
                    {
                        groupRightId = nodeId + ":" + "-1" + ":" + eachNodeRights.type;
                    }


                    if (eachNodeRights.type == "owner")//Owner
                    {
                        if (eachNodeRights.right_id != null)
                        {
                            Console.WriteLine(groupName);

                            if (!treeView_access_list.Nodes.ContainsKey(defaultAccess))
                            {
                                TreeNode defaultNode = new TreeNode(defaultAccess);
                                defaultNode.Name = defaultAccess;
                                treeView_access_list.Nodes.Add(defaultNode);
                            }

                            treeView_access_list.Nodes[defaultAccess].Nodes.Add(groupRightId, groupName, 4, 4);
                        }

                    }
                    else if (eachNodeRights.type == "group") //Default access (Owner Group)
                    {
                        if (eachNodeRights.right_id != null)
                        {
                            Console.WriteLine(groupName);
                            if (!treeView_access_list.Nodes.ContainsKey(defaultAccess))
                            {
                                TreeNode defaultNode = new TreeNode(defaultAccess);
                                defaultNode.Name = defaultAccess;
                                treeView_access_list.Nodes.Add(defaultNode);
                            }

                            treeView_access_list.Nodes[defaultAccess].Nodes.Add(groupRightId, groupName, 5, 5);
                            //Show permissions
                        }

                    }
                    else if (eachNodeRights.type == "public") //Public access
                    {
                        Console.WriteLine(eachNodeRights.permissions);
                        Console.WriteLine(groupName);

                        if (eachNodeRights.permissions != null)
                        {
                            if (!treeView_access_list.Nodes.ContainsKey(defaultAccess))
                            {
                                TreeNode defaultNode = new TreeNode(defaultAccess);
                                defaultNode.Name = defaultAccess;
                                treeView_access_list.Nodes.Add(defaultNode);
                            }

                            treeView_access_list.Nodes[defaultAccess].Nodes.Add(groupRightId, "Public Access", 6, 6);

                        }
                    }
                    else if (eachNodeRights.type == "custom")//Assigned access
                    {

                        Console.WriteLine(eachNodeRights.permissions);

                        Console.WriteLine(groupName);
                        if (!treeView_access_list.Nodes.ContainsKey(assignedAccess))
                        {
                            TreeNode assignedNode = new TreeNode(assignedAccess);
                            assignedNode.Name = assignedAccess;
                            treeView_access_list.Nodes.Add(assignedAccess);
                        }

                        if (eachNodeRights.right_id_expand.type_name != null)
                        {
                            if (eachNodeRights.right_id_expand.type_name == "User")
                            {
                                treeView_access_list.Nodes[assignedAccess].Nodes.Add(groupRightId, groupName, 4, 4);

                            }
                            else
                            {
                                treeView_access_list.Nodes[assignedAccess].Nodes.Add(groupRightId, groupName, 5, 5);

                            }
                        }
                    }

                }

                this.treeView_access_list.ExpandAll();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
            }
        }


        //Display Permission for each group
        private void treeView_access_list_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                Console.WriteLine(this.treeView_access_list.SelectedNode.Name);
                Console.WriteLine(this.treeView_access_list.SelectedNode.Text);

                dynamic groupsPermissions = null;

                if (General.otdsTicket == null)
                {
                    General.otdsTicket = businessLayerRestAPI.authenticateUserRest(General.username, General.password);
                }
                else if (!businessLayerRestAPI.isSessionActive(General.otdsTicket))
                {
                    General.otdsTicket = businessLayerRestAPI.authenticateUserRest(General.username, General.password);
                }

                ClearAllPermissionCheckBoxes();

                if (this.treeView_access_list.SelectedNode.Name != null)
                {
                    if ((this.treeView_access_list.SelectedNode.Name != "Default Access"))
                    {
                        if (this.treeView_access_list.SelectedNode.Name != "Assigned Access")
                        {
                            var splittedVal = this.treeView_access_list.SelectedNode.Name.Split(':');

                            if (splittedVal[0] != null)
                            {
                                long nodeId = long.Parse(splittedVal[0].ToString());
                                if (splittedVal[1] != null)
                                {
                                    long rightId = long.Parse(splittedVal[1].ToString());

                                    if (splittedVal[2] != null)
                                    {
                                        if (splittedVal[2].ToString() == "owner")
                                        {
                                            groupsPermissions = businessLayerRestAPI.getNodePermission(General.otdsTicket, nodeId, "permissions/" + splittedVal[2].ToString());

                                        }
                                        else if (splittedVal[2].ToString() == "group")
                                        {
                                            groupsPermissions = businessLayerRestAPI.getNodePermission(General.otdsTicket, nodeId, "permissions/" + splittedVal[2].ToString());

                                        }
                                        else if (splittedVal[2].ToString() == "public")
                                        {
                                            groupsPermissions = businessLayerRestAPI.getNodePermission(General.otdsTicket, nodeId, "permissions/" + splittedVal[2].ToString());
                                        }
                                        else if (splittedVal[2].ToString() == "custom")
                                        {
                                            groupsPermissions = businessLayerRestAPI.getNodePermission(General.otdsTicket, nodeId, "permissions/" + splittedVal[2].ToString() + "/" + rightId);

                                        }

                                        if (groupsPermissions != null)
                                        {
                                            //Display Checkbox
                                            foreach (var eachPermission in groupsPermissions.results.data.permissions.permissions)
                                            {
                                                string permission = eachPermission;
                                                if (permission == "see")
                                                {
                                                    checkBox_see.Checked = true;
                                                }
                                                else if (permission == "see_contents")
                                                {
                                                    checkBox_SeeContents.Checked = true;
                                                }
                                                else if (permission == "modify")
                                                {
                                                    checkBox_Modify.Checked = true;
                                                }
                                                else if (permission == "edit_attributes")
                                                {
                                                    checkBox_EditAttributes.Checked = true;
                                                }
                                                else if (permission == "add_items")
                                                {
                                                    checkBox_AddItems.Checked = true;
                                                }
                                                else if (permission == "reserve")
                                                {
                                                    checkBox_Reserve.Checked = true;
                                                }
                                                else if (permission == "delete_versions")
                                                {
                                                    checkBox_DeleteVersions.Checked = true;
                                                }
                                                else if (permission == "delete")
                                                {
                                                    checkBox_Delete.Checked = true;
                                                }
                                                else if (permission == "edit_permissions")
                                                {
                                                    checkBox_EditPermissions.Checked = true;
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }

                    }


                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
            }
        }

        //Clear all checkbox:
        private void ClearAllPermissionCheckBoxes()
        {
            try
            {
                checkBox_see.Checked = false;
                checkBox_SeeContents.Checked = false;
                checkBox_Modify.Checked = false;
                checkBox_EditAttributes.Checked = false;
                checkBox_AddItems.Checked = false;
                checkBox_Reserve.Checked = false;
                checkBox_DeleteVersions.Checked = false;
                checkBox_Delete.Checked = false;
                checkBox_EditPermissions.Checked = false;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
            }
        }

        //Export Button
        private void button_export_Click(object sender, EventArgs e)
        {
            try
            {
                if (General.parentFolderId == gEnterPriseWSNodeId)
                {
                    label_status_update.Text = "You can't run this on Enterprise level!";
                    MessageBox.Show("You can't run this on Enterprise level!");
                    return;
                }
                if (General.parentFolderId == 0)
                {
                    label_status_update.Text = "Please Select Content Server Parent Folder ID !";
                    MessageBox.Show("Please Select Content Server Parent Folder ID !");
                    return;
                }
                if (csvFilePath == null)
                {
                    label_status_update.Text = "Select Destination Folder!";
                    MessageBox.Show("Select Destination Folder!");
                    return;
                }
                else
                {
                    this.button_cancel.Enabled = true;
                    this.button_export.Enabled = false;

                    ////Disable/lock these controls 
                    this.button_browseFolder.Enabled = false;

                    this.pictureBox_loading.Visible = true;
                    this.pictureBox_loading.Enabled = true;

                    List<object> bgWorkerArguments = new List<object>();
                    bgWorkerArguments.Add(General.parentFolderId);
                    bgWorkerArguments.Add(csvFilePath);
                    bgWorkerArguments.Add(this.treeView_cs_list_contents.SelectedNode.Text);


                    //Call bgworker
                    if (bgWorker_permissionExporter.IsBusy != true)
                    {
                        // Start the asynchronous operation.
                        bgWorker_permissionExporter.RunWorkerAsync(bgWorkerArguments);
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

            }
        }

        //Background worker:

        public BackgroundWorker worker;
        private void bgWorker_permissionExporter_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                worker = sender as BackgroundWorker;

                if (worker.CancellationPending == true)
                {
                    e.Cancel = true;

                }
                else
                {
                    // Perform a time consuming operation and report progress.
                    System.Threading.Thread.Sleep(500);
                    List<object> genericVarList = e.Argument as List<object>;

                    exportFlag = PermissionExporter(long.Parse(genericVarList[0].ToString()), genericVarList[1].ToString(), e, worker);

                    if (exportFlag)
                    {
                        //ExportPermissions exportPermissions = new ExportPermissions();

                        //Then Export to CSV
                        exportSuccessFlag = ExportPermissions.CSVXport(genericVarList[2].ToString(), csvFilePath, long.Parse(genericVarList[0].ToString()), permissionsValue);

                        Console.WriteLine(exportSuccessFlag);

                    }


                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
            }
        }

        private void bgWorker_permissionExporter_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            label_status_update.Text = ("Exporting Permissions Data . . .");
        }

        private void bgWorker_permissionExporter_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == true)
            {
                label_status_update.Text = "Canceled!";

                log.Info("---------------------------|Batch Canceled|---------------------------------------------");
                log.Info(DateTime.Now + " | Batch Process Canceled");
                //log.Info("Total Created Folders     : " + General.folderCreatedCount.ToString());
                //log.Info("Total Existing Folders    : " + General.folderExistingCount.ToString());

                log.Info("---------------------------------------------------------------------------------------");

            }
            else if (e.Error != null)
            {
                label_status_update.Text = "Error: " + e.Error.Message;
            }
            else
            {
                if (cancelFlag)
                {
                    this.pictureBox_loading.Enabled = false;
                    label_status_update.Text = "Canceled!";

                    log.Info("---------------------------|Batch Canceled|---------------------------------------------");
                    log.Info(DateTime.Now + " | Batch Process Canceled");
                    //log.Info("Total Created Folders     : " + General.folderCreatedCount.ToString());
                    //log.Info("Total Existing Folders    : " + General.folderExistingCount.ToString());

                    log.Info("---------------------------------------------------------------------------------------");
                    return;
                }
                if (exportSuccessFlag)
                {
                    //Log
                    log.Info("---------------------------|Batch Summary|---------------------------------------------");
                    log.Info(DateTime.Now + " | Batch Process Completed");
                    log.Info("Total Exported Documents     : " + General.rowCounter.ToString());
                    //log.Info("Total Existing Folders    : " + General.folderExistingCount.ToString());

                    log.Info("---------------------------------------------------------------------------------------");

                    label_status_update.Text = "Done!";
                    label_status_update.Text = "Process Completed...";

                    this.pictureBox_loading.Enabled = false;
                    MessageBox.Show("Permissions are exported!", "Process Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                else
                {
                    //Log
                    log.Info("---------------------------|Batch Summary|---------------------------------------------");
                    log.Info(DateTime.Now + " | Export Failed");
                    log.Info("Total Exported Documents     : " + General.rowCounter.ToString());
                    //log.Info("Total Existing Folders    : " + General.folderExistingCount.ToString());

                    log.Info("---------------------------------------------------------------------------------------");

                    label_status_update.Text = "Export Failed!";

                    this.pictureBox_loading.Enabled = false;
                    MessageBox.Show("Permission Export Failed!", "Export Failed", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                }

            }
            ClearNClose();
        }

        //Using Delegates:
        public delegate void SetLabelText_Delegate(Label Label, string text);

        // The delegates subroutine.
        public void SetLabelText_ThreadSafe(Label Label, string text)
        {
            // InvokeRequired required compares the thread ID of the calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (Label.InvokeRequired)
            {
                SetLabelText_Delegate MyDelegate = new SetLabelText_Delegate(SetLabelText_ThreadSafe);
                this.Invoke(MyDelegate, new object[] {
                    Label,
                    text
                });
            }
            else
            {
                Label.Text = text;
            }
        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            try
            {
                cancelFlag = true;

                if (bgWorker_permissionExporter.WorkerSupportsCancellation == true)
                {
                    // Cancel the asynchronous operation.
                    this.bgWorker_permissionExporter.CancelAsync();

                }
                //Application.OpenForms["Login"].Close();

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

            }
        }

        private void button_browseFolder_Click(object sender, EventArgs e)
        {
            try
            {
                //openFileDialog_excel_file.ShowDialog();
                if (folderBrowserDialog_csv.ShowDialog() == DialogResult.OK)
                {
                    csvFilePath = folderBrowserDialog_csv.SelectedPath.ToString();
                    textBox_destinationFolder.Text = csvFilePath;
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

            }
        }

        private void ClearNClose()
        {
            this.pictureBox_loading.Visible = false;
            this.pictureBox_loading.Enabled = false;

            this.button_cancel.Enabled = false;
            this.button_export.Enabled = true;

            ////Disable/lock these controls 
            this.button_browseFolder.Enabled = true;

            long level1Id = 0;
            long level2Id = 0;
            long level3Id = 0;
            long level4Id = 0;
            long level5Id = 0;
            long level6Id = 0;
            long level7Id = 0;
            long level8Id = 0;
            long level9Id = 0;


            ColumnNameWithNo = new Dictionary<string, int>();
            General.rowCounter = 0;

            permissionsValue = null;
            exportFlag = false;
            exportSuccessFlag = false;
            cancelFlag = false;
        }


        private DocumentManagementClient docMan = new DocumentManagementClient();
        long level1Id = 0;
        long level2Id = 0;
        long level3Id = 0;
        long level4Id = 0;
        long level5Id = 0;
        long level6Id = 0;
        long level7Id = 0;
        long level8Id = 0;
        long level9Id = 0;

        private bool PermissionExporter(long nodeId, string csvFilePath, DoWorkEventArgs e, BackgroundWorker worker)
        {
            
            try
            {
                ExportPermissions.BuildColumns();
                if (nodeId != 0)
                {
                    level1Id = nodeId;

                   Node currentNode = docMan.GetNode(ref OTAuth, nodeId);
                    if (currentNode.Type == "Folder")
                    {
                        //List all the nodes recursively
                        ListNodesRecursively(currentNode, e, worker);
                    }
                    else if(currentNode.Type == "Document")
                    {
                        listPermissions(currentNode);
                    }
                   
                }

                return true;
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                return false;
            }
            finally
            {
                docMan.Close();
            }
        }

        private long ListNodesRecursively(Node node, DoWorkEventArgs e, BackgroundWorker worker)
        {
            long nodeCount = 0;
            try
            {   
                Node[] ListofNodes = new Node[] { };
                ListofNodes = docMan.ListNodes(ref OTAuth, node.ID, true);

                //Get Parent node permissions
                NodeRights level1NodePermissions = docMan.GetNodeRights(ref OTAuth, node.ID);
               

                if (ListofNodes != null)
                {
                    foreach (var nodes in ListofNodes.OfType<Node>().OrderBy(n => n.Type)) //Type bz Document comes first and then Folder
                    {
                        //NodePermissions nodePermissions = nodes.Permissions;
                        //NodeRights test = docMan.GetNodeRights(ref OTAuth, nodes.ID);
                        //if (level1NodePermissions != test)
                        //{
                        //    Console.WriteLine("Node ID: " + nodes.ID + "| Node Name: " + nodes.Name + " is not same");

                        //}
                        //else
                        //{
                        //    Console.WriteLine("Node ID: " + nodes.ID + "| Node Name: " + nodes.Name + " is same");

                        //}

                        //Parent folder infor - Test
                        //listPermissions(nodes.ID);
                        //Parent folder infor - Test

                        if (nodes.Type == "Folder")
                        {                            
                            //Recursive
                            ListNodesRecursively(nodes, e, worker);
                        }
                        else if (nodes.Type == "Document")
                        {

                            //Get permission values
                            listPermissions(nodes);
                        }
                    }
                }

                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                return 0;
            }
        }

        private void listPermissions(Node node)
        {
            bool flag2Continue = false;

            //comparing node permssion with it's parent
            if (checkBox_listExceptions.Checked)
            {
                //Get node rights
                NodeRights currentNodeRights =  docMan.GetNodeRights(ref OTAuth, node.ID);

               
                //Get parent rights
                NodeRights parentNodeRights = docMan.GetNodeRights(ref OTAuth, node.ParentID);

                //compare
                //if !true  flag2Continue != true
                if (currentNodeRights != null)
                {
                    if (parentNodeRights !=null)
                    {
                        if (currentNodeRights == parentNodeRights)
                        {
                            flag2Continue = true;
                        }
                    }
                }

            }


            //Each doc is a row
            General.rowCounter = General.rowCounter + 1;

            try
            {
                dynamic groupsPermissionsList = null;
                dynamic groupsPermissions = null;
                string defaultAccess = "Default Access";
                string assignedAccess = "Assigned Access";

                string permissionBitMask = null;
                int columnNumber = 0;

                if (General.otdsTicket == null)
                {
                    General.otdsTicket = businessLayerRestAPI.authenticateUserRest(General.username, General.password);
                }
                else if (!businessLayerRestAPI.isSessionActive(General.otdsTicket))
                {
                    General.otdsTicket = businessLayerRestAPI.authenticateUserRest(General.username, General.password);
                }

                string nodePath = BusinessLayer.CreatePath(int.Parse(node.ID.ToString()), OTAuth);
                if (nodePath.Contains(','))
                {
                    nodePath = nodePath.Replace(',', ' ');
                }

                string nodeName = node.Name;
                if (nodeName.Contains(','))
                {
                    nodeName = nodeName.Replace(',', ' ');
                }

                SetLabelText_ThreadSafe(label_status_update, "Exporting : " + nodePath);

                Console.WriteLine("Node ID: " + node.ID);
                Console.WriteLine("Node Name: " + node.Name);
                Console.WriteLine("Node Parent ID: " + node.ParentID);
                Console.WriteLine("Node Path: " + nodePath);
                Console.WriteLine("Node Type: " + node.Type);

                //adding document columns
                ExportPermissions.CSVSave(node.ID.ToString(), 0, (General.rowCounter));              
                ExportPermissions.CSVSave(nodeName, 1, (General.rowCounter));
                ExportPermissions.CSVSave(node.ParentID.ToString(), 2, (General.rowCounter));
                ExportPermissions.CSVSave(nodePath, 3, (General.rowCounter));
                ExportPermissions.CSVSave(node.Type, 4, (General.rowCounter));

                //Permissions
                //ExportPermissions.CSVSave("Test", 5, (General.rowCounter));
                //ExportPermissions.CSVSave("Test", 6, (General.rowCounter));
                //ExportPermissions.CSVSave("Test", 7, (General.rowCounter));
                //ExportPermissions.CSVSave("Test", 8, (General.rowCounter));


                //Get Permissions
                groupsPermissionsList = businessLayerRestAPI.getNodePermissionsList(General.otdsTicket, node.ID);

                int counter = 0;
                foreach (var eachNodeRights in groupsPermissionsList.results.data.permissions)
                {
                    string groupName = null;
                    string groupRightId = null;
                    string type = null;

                    if (eachNodeRights.type != "public")
                    {
                        if (eachNodeRights.right_id_expand != null)
                        {
                            if (eachNodeRights.right_id_expand.name != null)
                            {
                                groupName = eachNodeRights.right_id_expand.name;
                                groupRightId = eachNodeRights.right_id_expand.id ;

                                if (eachNodeRights.right_id_expand.type_name !=null)
                                {
                                    type = eachNodeRights.right_id_expand.type_name;
                                }
                                
                            }
                        }

                    }
                    else
                    {
                        groupRightId = "-1";
                    }

                   

                    if (eachNodeRights.type == "owner")//Owner
                    {
                        columnNumber = 5;
                        if (eachNodeRights.right_id != null)
                        {
                            Console.WriteLine(groupName);
                            
                            //Get permission bit
                            groupsPermissions = businessLayerRestAPI.getNodePermission(General.otdsTicket, node.ID, "permissions/" + "owner");

                            permissionBitMask = FormatePermissionsCellValue("Owner", groupName, groupsPermissions, type);
                        }

                    }
                    else if (eachNodeRights.type == "group") //Default access (Owner Group)
                    {
                        columnNumber = 6;
                        
                        if (eachNodeRights.right_id != null)
                        {
                            Console.WriteLine(groupName);
                         
                            //Get permission bit
                            groupsPermissions = businessLayerRestAPI.getNodePermission(General.otdsTicket, node.ID, "permissions/" + "group");
                            permissionBitMask = FormatePermissionsCellValue("Owner Group", groupName, groupsPermissions, type);

                        }

                    }
                    else if (eachNodeRights.type == "public") //Public access
                    {
                        Console.WriteLine(eachNodeRights.permissions);
                        Console.WriteLine(groupName);
                        columnNumber = 7;


                        if (eachNodeRights.permissions != null)
                        {
                            permissionBitMask = "Public Access" + groupName;

                            //Get permission bit
                            groupsPermissions = businessLayerRestAPI.getNodePermission(General.otdsTicket, node.ID, "permissions/" + "public");

                            permissionBitMask = FormatePermissionsCellValue("Public", "Public Access", groupsPermissions, type);

                        }
                    }
                    else if (eachNodeRights.type == "custom")//Assigned access
                    {
                        counter = counter + 1;
                        string customGroupPermissions = null;
                        columnNumber = 8;

                        Console.WriteLine(eachNodeRights.permissions);
                        Console.WriteLine(groupName);
                       

                        //Get permission bit
                        groupsPermissions = businessLayerRestAPI.getNodePermission(General.otdsTicket, node.ID, "permissions/" + "custom" + "/" + groupRightId);

                        customGroupPermissions = FormatePermissionsCellValue("Assigned Access", groupName, groupsPermissions, type);

                        if (counter == 1)
                        {
                            permissionBitMask = customGroupPermissions;
                        }
                        else
                        {
                            permissionBitMask = permissionBitMask + " || " + customGroupPermissions;

                        }
                    }


                    //Saving Attribute Value RowCounter
                    ExportPermissions.CSVSave(permissionBitMask, columnNumber, (General.rowCounter)); //I guess no need to do this  (RowCounter - 1) bz 0th row is column header

                    //Nullify column no:
                    columnNumber = 0;
                }
                        
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
            }
        }

        private void ContentServerACL_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.OpenForms["Login"].Close();

        }

        private static string FormatePermissionsCellValue(string rightType, string rightName, dynamic groupsPermissions, string groupOrUserType)
        {
            string returnValue = null;
            string permissionsBit = null;
            var permissionAsc = new List<KeyValuePair<int, string>>();
            try
            {
                if (groupsPermissions != null)
                {
                    foreach (var eachPermission in groupsPermissions.results.data.permissions.permissions)
                    {
                        

                        string permission = eachPermission;

                        
                        if (permission == "see")
                        {                            
                            permissionAsc.Add(new KeyValuePair<int, string>(1, "see"));
                        }
                        else if (permission == "see_contents")
                        {
                            permissionAsc.Add(new KeyValuePair<int, string>(2, "see_contents"));
                        }
                        else if (permission == "modify")
                        {
                            permissionAsc.Add(new KeyValuePair<int, string>(3, "modify"));
                        }
                        else if (permission == "edit_attributes")
                        {
                            permissionAsc.Add(new KeyValuePair<int, string>(4, "edit_attributes"));
                        }
                        else if (permission == "add_items")
                        {
                            permissionAsc.Add(new KeyValuePair<int, string>(5, "add_items"));
                        }
                        else if (permission == "reserve")
                        {
                            permissionAsc.Add(new KeyValuePair<int, string>(6, "reserve"));
                        }
                        else if (permission == "delete_versions")
                        {
                            permissionAsc.Add(new KeyValuePair<int, string>(7, "delete_versions"));
                        }
                        else if (permission == "delete")
                        {
                            permissionAsc.Add(new KeyValuePair<int, string>(8, "delete"));
                        }
                        else if (permission == "edit_permissions")
                        {
                            permissionAsc.Add(new KeyValuePair<int, string>(9, "edit_permissions"));
                        }
                    }

                    int counter = 0;
                   
                    foreach (var item in permissionAsc.OrderBy(x => x.Key))
                    {
                        counter = counter + 1;
                        if (counter == 1)
                        {
                            permissionsBit = item.Value;
                        }
                        else
                        {
                            permissionsBit = permissionsBit + " | " + item.Value;

                        }
                    }
                    //returnValue = "Access Type: " + rightType + " | Group/User Name: " + rightName + " | Permissions: {" + permissionsBit + "}";

                    if (groupOrUserType == null)
                    {
                        returnValue = rightName + " - Permissions: {" + permissionsBit + "}";

                    }
                    else
                    {
                        returnValue = groupOrUserType + ": " + rightName + " - Permissions: {" + permissionsBit + "}";
                        
                    }

                }

                return returnValue;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                return returnValue;
            }

            
        }
    }
}
