﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Configuration;
using Newtonsoft.Json;
using System.Windows.Forms;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.ServiceModel;

namespace CISModelGenerator
{
    class Utils
    {
        private static readonly log4net.ILog log =
           log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void ReadAllSettings()
        {
            string TempStr_RefInterval = ConfigurationManager.AppSettings["RefreshInterval"];
            int OBJId;
            bool isNumeric = false;
            //Validating entered string Value is integer or NOT
            isNumeric = int.TryParse(TempStr_RefInterval, out OBJId);
            if (isNumeric)
            {
                if (double.Parse(TempStr_RefInterval) < 10)
                {
                    General.gDouble_RefreshInterval = Utils.Min2MilliSec(10); //By Default on every 10 min, token is being refreshed
                }
                else

                {
                    General.gDouble_RefreshInterval = Utils.Min2MilliSec(double.Parse(TempStr_RefInterval));
                }
            }
            else
            {
                General.gDouble_RefreshInterval = Utils.Min2MilliSec(10); //By Default on every 10 min, token is being refreshed
            }

            
            General.cSRestApiUrl = ConfigurationManager.AppSettings["CSRestApi"].TrimStart().TrimEnd();
        }
        //Get Permission types:
        public static List<string> GetPermissionList()
        {
            List<string> returnType = new List<string>();

            try
            {
                if (General.gstrPermissionMappings != null)
                {
                    foreach (var permissionItem in General.gstrPermissionMappings)
                    {
                        string permissionType = ((Newtonsoft.Json.Linq.JProperty)permissionItem).Name.ToString();
                        returnType.Add(permissionType);
                    }
                }

                return returnType;
            }
            catch (FaultException ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                return returnType;
            }
            
        }
        public static void CreateLogFolder()
        {
            try
            {
                string FolderName = "log";
                System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + FolderName);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);

            }

        }

        //Timer Related Functions:--Start--
        public static bool CallTimerEvent()
        {
            try
            {
                System.Timers.Timer aTimer = new System.Timers.Timer();
                aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                aTimer.Interval = General.gDouble_RefreshInterval; //60000 miliseconds = 1 min | 900000 ms = 15 min
                aTimer.Enabled = true;

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                return false;
            }
        }
        // Specify what you want to happen when the Elapsed event is raised.
        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            try
            {
                //Session is being refreshed to re-assigned New Token
                BusinessLayer.UpdateAuthenication();
                System.Diagnostics.Debug.WriteLine("Session Refreshed and Assigned New Token!"); //
                System.Diagnostics.Debug.WriteLine("New Auth Toke: " + General.authToken);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
            }

        }

        public static double Min2MilliSec(double Minutes)
        {
            double RetVal = 0;

            try
            {
                TimeSpan InMilliSec = TimeSpan.FromMinutes(Minutes);

                return RetVal = InMilliSec.TotalMilliseconds;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                return RetVal;
            }
        }

        public static string SplitAndGetvalue(string inputValue, [Optional]  string valueName, [Optional] string valueIndex)
        {
            string returnValue = null;
            try
            {
                char excelDelimiter = char.Parse(General.gstrExcelDelimiter);

                if (inputValue.Contains(excelDelimiter))
                {
                    var splittedValue = inputValue.Split(excelDelimiter);

                    //case 1
                    if ((valueName == null) && (valueIndex != null))
                    {
                        //if (valueIndex != null || valueIndex != 0)
                        //{                        
                            returnValue = splittedValue[int.Parse(valueIndex)].ToString();
                            return returnValue;
                        //}
                    }

                    //case 2
                    if ((valueName != null) && (valueIndex == null))
                    {
                        foreach (var item in splittedValue)
                        {
                            if (item == valueName)
                            {
                                returnValue = item.ToString();
                                return returnValue;
                            }
                        }
                    }
                }

                return returnValue;
            }
            catch (Exception ex)
            {


                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                return returnValue;
            }
        }

        /// <summary>
        /// Encrypt and Decrypt Functions
        /// </summary>
        private static readonly byte[] fKey = ASCIIEncoding.ASCII.GetBytes("John3:16");

        /// <summary>
        /// Encrypt a string.
        /// </summary>
        /// <param name="originalString">The original string.</param>
        /// <returns>The encrypted string.</returns>
        /// <exception cref="ArgumentNullException">This exception will be thrown when the original string is null or empty.</exception>
        public static string Encrypt(string originalString)
        {
            string retval = null;


            if (String.IsNullOrEmpty(originalString))
            {
                retval = originalString;
            }
            else
            {
                try
                {
                    DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
                    MemoryStream memoryStream = new MemoryStream();
                    CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoProvider.CreateEncryptor(fKey, fKey), CryptoStreamMode.Write);

                    using (StreamWriter writer = new StreamWriter(cryptoStream))
                    {
                        writer.Write(originalString);
                        writer.Flush();
                        cryptoStream.FlushFinalBlock();
                        writer.Flush();

                        retval = Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
                    }
                }
                catch (CryptographicException ex)
                {
                    Console.WriteLine(ex.Message);

                    log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                    log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                }
            }

            return retval;
        }

        /// <summary>
        /// Decrypt a crypted string.
        /// </summary>
        /// <param name="cryptedString">The crypted string.</param>
        /// <returns>The decrypted string.</returns>
        /// <exception cref="ArgumentNullException">This exception will be thrown when the crypted string is null or empty.</exception>
        public static string Decrypt(string cryptedString)
        {
            string retval = null;


            if (String.IsNullOrEmpty(cryptedString))
            {
                retval = cryptedString;
            }
            else
            {
                try
                {
                    DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
                    MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(cryptedString));
                    CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoProvider.CreateDecryptor(fKey, fKey), CryptoStreamMode.Read);

                    using (StreamReader reader = new StreamReader(cryptoStream))
                    {
                        retval = reader.ReadToEnd();
                    }
                }
                catch (CryptographicException ex)
                {
                    Console.WriteLine(ex.Message);

                    log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                    log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                }
            }

            return retval;
        }


        //Validate Licensing
        public static void ValidateLicense()
        {
           
            try
            {
                //Generate Rkey file| License file
                //If file already exists then read only  RKey parameter
                if (File.Exists(Application.StartupPath + "\\RKey.xml.config"))
                {

                    //- Just read RKey parameter &
                    //- Validate License
                    string RkeyValue =null;
                    var fileConfig = ConfigurationManager.OpenExeConfiguration(Application.StartupPath + "\\RKey.xml");
                    var settings = fileConfig.AppSettings.Settings;
                    if (settings["RKey"] == null)
                    {
                        RkeyValue = null;
                    }
                    else
                    {
                        RkeyValue = settings["RKey"].Value.ToString();
                    }
                

                    System.Diagnostics.Debug.WriteLine("Rkey Value : " + RkeyValue);

                    //General.LicenseValidate(RkeyValue);
                    //
                    string MacID = null;
                    string UserID = null;
                    string HostName = null;
                    string SystemUniqueFingerPrint = null;

                    MacID = GetMACID();
                    UserID = GetLoggedInUserID();
                    HostName = GetHostName();

                    SystemUniqueFingerPrint = "MacID: " + MacID + " | HostName: " + HostName + " | UserID: " + UserID;


                    System.Diagnostics.Debug.WriteLine("Decrypted Value : " + Decrypt(RkeyValue));
                    ///--//
                    if (Decrypt(RkeyValue) == "none" || RkeyValue == "uDSVVxeisUs=") //"uDSVVxeisUs=" means none
                    {
                        General.validLicense = false;
                    }
                    else
                    {
                        Console.WriteLine("uncrypted Value : " + SystemUniqueFingerPrint.ToLower().ToString());
                        Console.WriteLine("decrypted Value : " + Decrypt(RkeyValue).ToLower().ToString());

                        if (SystemUniqueFingerPrint.ToLower().ToString() == Decrypt(RkeyValue).ToLower().ToString())
                        {
                            //Validated
                           General.validLicense = true;
                            //return true;
                        }
                        else
                        {
                            General.validLicense = false;

                        }
                    }

                    System.Diagnostics.Debug.WriteLine("Recursive Validated : " + General.validLicense);
                }
                else
                {

                    ////generate Rkey file| License file
                    GenerateRKeyConfig("App", "CISModelGenerator");
                    GenerateRKeyConfig("FingerPrint1", Encrypt(GetMACID()));
                    GenerateRKeyConfig("FingerPrint2", Encrypt(GetHostName()));
                    GenerateRKeyConfig("FingerPrint3", Encrypt(GetLoggedInUserID()));
                    GenerateRKeyConfig("RKey", "uDSVVxeisUs="); // "uDSVVxeisUs=" means none
                }

            }
            catch (Exception ex)
            {
                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
            }
        }

        //This is kind of hack : Bz using default function to write/generate own license key :)
        public static void GenerateRKeyConfig(string key, string value)
        {
            try
            {
                if (!(File.Exists(Application.StartupPath + "\\RKey.xml")))
                {
                    //Create empty and dummy xml file
                    File.Create(Application.StartupPath + "\\RKey.xml"); // Empty 


                    var configFile = ConfigurationManager.OpenExeConfiguration(Application.StartupPath + "\\RKey.xml");
                    var settings = configFile.AppSettings.Settings;
                    if (settings[key] == null)
                    {
                        settings.Add(key, value);
                    }
                    else
                    {
                        settings[key].Value = value;
                    }
                    configFile.Save(ConfigurationSaveMode.Modified);
                    ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);

                }
                else
                {
                    var configFile = ConfigurationManager.OpenExeConfiguration(Application.StartupPath + "\\RKey.xml");
                    var settings = configFile.AppSettings.Settings;
                    if (settings[key] == null)
                    {
                        settings.Add(key, value);
                    }
                    else
                    {
                        settings[key].Value = value;
                    }
                    configFile.Save(ConfigurationSaveMode.Modified);
                    ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
                }

            }
            catch (ConfigurationErrorsException ex)
            {
                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

            }
        }

        public static string ReadRKeyConfig(string key)
        {
            try
            {
                string RetVal;
                var configFile = ConfigurationManager.OpenExeConfiguration(Application.StartupPath + "\\RKey.xml");
                var settings = configFile.AppSettings.Settings;
                if (settings[key] == null)
                {
                    RetVal = null;
                }
                else
                {
                    RetVal = settings[key].Value.ToString();
                }


                return RetVal;
            }
            catch (ConfigurationErrorsException ex)
            {
                log.Info(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                return null;
            }
        }


        public static string GetMACID()
        {
            try
            {
                NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
                string sMacAddress = string.Empty;
                foreach (NetworkInterface adapter in nics)
                {
                    if (sMacAddress == String.Empty)// only return MAC Address from first card  
                    {
                        IPInterfaceProperties properties = adapter.GetIPProperties();
                        sMacAddress = adapter.GetPhysicalAddress().ToString();
                    }
                }
                return sMacAddress;

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error : " + ex.Message);
                return null;
            }

        }
        public static string GetLoggedInUserID()
        {

            try
            {
                //string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                //OR
                string userName1 = Environment.UserName;

                return userName1;

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error : " + ex.Message);
                return null;
            }

        }

        public static string GetHostName()
        {

            try
            {
                //string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                //OR
                string hostName = System.Environment.GetEnvironmentVariable("COMPUTERNAME");

                return hostName;


            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error : " + ex.Message);
                return null;
            }

        }
    }
}
