﻿namespace CISModelGenerator
{
    partial class BrowseLL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BrowseLL));
            this.treeView_cs_list_contents = new System.Windows.Forms.TreeView();
            this.imageList_LivelinkTreeIcons = new System.Windows.Forms.ImageList(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // treeView_cs_list_contents
            // 
            this.treeView_cs_list_contents.ImageIndex = 2;
            this.treeView_cs_list_contents.ImageList = this.imageList_LivelinkTreeIcons;
            this.treeView_cs_list_contents.Location = new System.Drawing.Point(12, 12);
            this.treeView_cs_list_contents.Name = "treeView_cs_list_contents";
            this.treeView_cs_list_contents.SelectedImageIndex = 2;
            this.treeView_cs_list_contents.Size = new System.Drawing.Size(784, 725);
            this.treeView_cs_list_contents.TabIndex = 1;
            this.treeView_cs_list_contents.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_cs_list_contents_AfterSelect);
            // 
            // imageList_LivelinkTreeIcons
            // 
            this.imageList_LivelinkTreeIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_LivelinkTreeIcons.ImageStream")));
            this.imageList_LivelinkTreeIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList_LivelinkTreeIcons.Images.SetKeyName(0, "folder16.gif");
            this.imageList_LivelinkTreeIcons.Images.SetKeyName(1, "folder16_.png");
            this.imageList_LivelinkTreeIcons.Images.SetKeyName(2, "mime_folder.png");
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(612, 744);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(184, 58);
            this.button1.TabIndex = 2;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // BrowseLL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(808, 821);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.treeView_cs_list_contents);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "BrowseLL";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BrowseLL";
            this.Load += new System.EventHandler(this.BrowseLL_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeView_cs_list_contents;
        private System.Windows.Forms.ImageList imageList_LivelinkTreeIcons;
        private System.Windows.Forms.Button button1;
    }
}