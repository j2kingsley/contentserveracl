﻿namespace CISModelGenerator
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.btn_Login = new System.Windows.Forms.Button();
            this.label_status = new System.Windows.Forms.Label();
            this.button_cancel = new System.Windows.Forms.Button();
            this.textBox_cws_url = new System.Windows.Forms.TextBox();
            this.label_cws_url = new System.Windows.Forms.Label();
            this.label_password = new System.Windows.Forms.Label();
            this.label_username = new System.Windows.Forms.Label();
            this.textBox_password = new System.Windows.Forms.TextBox();
            this.textBox_username = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Login
            // 
            this.btn_Login.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_Login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Login.Location = new System.Drawing.Point(663, 270);
            this.btn_Login.Margin = new System.Windows.Forms.Padding(8);
            this.btn_Login.Name = "btn_Login";
            this.btn_Login.Size = new System.Drawing.Size(248, 54);
            this.btn_Login.TabIndex = 13;
            this.btn_Login.Text = "Login";
            this.btn_Login.UseVisualStyleBackColor = true;
            this.btn_Login.Click += new System.EventHandler(this.btn_Login_Click);
            // 
            // label_status
            // 
            this.label_status.AutoSize = true;
            this.label_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_status.Location = new System.Drawing.Point(55, 64);
            this.label_status.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label_status.MaximumSize = new System.Drawing.Size(1100, 0);
            this.label_status.Name = "label_status";
            this.label_status.Size = new System.Drawing.Size(0, 31);
            this.label_status.TabIndex = 1;
            // 
            // button_cancel
            // 
            this.button_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_cancel.Location = new System.Drawing.Point(925, 270);
            this.button_cancel.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(248, 54);
            this.button_cancel.TabIndex = 14;
            this.button_cancel.Text = "Cancel";
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // textBox_cws_url
            // 
            this.textBox_cws_url.Location = new System.Drawing.Point(533, 40);
            this.textBox_cws_url.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.textBox_cws_url.Name = "textBox_cws_url";
            this.textBox_cws_url.Size = new System.Drawing.Size(636, 38);
            this.textBox_cws_url.TabIndex = 9;
            this.textBox_cws_url.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_cws_url_KeyPress);
            // 
            // label_cws_url
            // 
            this.label_cws_url.AutoSize = true;
            this.label_cws_url.Location = new System.Drawing.Point(75, 40);
            this.label_cws_url.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label_cws_url.Name = "label_cws_url";
            this.label_cws_url.Size = new System.Drawing.Size(438, 32);
            this.label_cws_url.TabIndex = 17;
            this.label_cws_url.Text = "CWS Authentication service URL:";
            // 
            // label_password
            // 
            this.label_password.AutoSize = true;
            this.label_password.Location = new System.Drawing.Point(75, 183);
            this.label_password.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label_password.Name = "label_password";
            this.label_password.Size = new System.Drawing.Size(147, 32);
            this.label_password.TabIndex = 16;
            this.label_password.Text = "Password:";
            // 
            // label_username
            // 
            this.label_username.AutoSize = true;
            this.label_username.Location = new System.Drawing.Point(75, 111);
            this.label_username.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label_username.Name = "label_username";
            this.label_username.Size = new System.Drawing.Size(164, 32);
            this.label_username.TabIndex = 15;
            this.label_username.Text = "User Name:";
            // 
            // textBox_password
            // 
            this.textBox_password.Location = new System.Drawing.Point(533, 183);
            this.textBox_password.Margin = new System.Windows.Forms.Padding(8);
            this.textBox_password.Name = "textBox_password";
            this.textBox_password.PasswordChar = '*';
            this.textBox_password.Size = new System.Drawing.Size(636, 38);
            this.textBox_password.TabIndex = 12;
            this.textBox_password.UseSystemPasswordChar = true;
            this.textBox_password.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_password_KeyPress);
            // 
            // textBox_username
            // 
            this.textBox_username.Location = new System.Drawing.Point(533, 111);
            this.textBox_username.Margin = new System.Windows.Forms.Padding(8);
            this.textBox_username.Name = "textBox_username";
            this.textBox_username.Size = new System.Drawing.Size(636, 38);
            this.textBox_username.TabIndex = 10;
            this.textBox_username.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_username_KeyPress);
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this.label_status);
            this.groupBox1.Location = new System.Drawing.Point(81, 340);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(8);
            this.groupBox1.Size = new System.Drawing.Size(1092, 141);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Status :";
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1263, 519);
            this.Controls.Add(this.btn_Login);
            this.Controls.Add(this.button_cancel);
            this.Controls.Add(this.textBox_cws_url);
            this.Controls.Add(this.label_cws_url);
            this.Controls.Add(this.label_password);
            this.Controls.Add(this.label_username);
            this.Controls.Add(this.textBox_password);
            this.Controls.Add(this.textBox_username);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.Load += new System.EventHandler(this.button_login_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Login;
        private System.Windows.Forms.Label label_status;
        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.TextBox textBox_cws_url;
        private System.Windows.Forms.Label label_cws_url;
        private System.Windows.Forms.Label label_password;
        private System.Windows.Forms.Label label_username;
        private System.Windows.Forms.TextBox textBox_password;
        private System.Windows.Forms.TextBox textBox_username;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}

