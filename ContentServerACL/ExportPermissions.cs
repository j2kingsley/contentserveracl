﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CISModelGenerator
{
   public class ExportPermissions
    {
        private static readonly log4net.ILog log =
       log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static CSVManipulation csv = new CSVManipulation();
        public static StringBuilder sb = new StringBuilder();

        //Filename and ObjId column
        public static void BuildColumns()
        {
            //sb.Append("$ObjectID");
            //sb.Append(",");
            //sb.Append("$ObjectName");
            //sb.Append(",");
            //sb.Append("$ObjectParent");
            //sb.Append(",");
            //sb.Append("$ObjectPath");
            //sb.Append(",");
            //sb.Append("$ObjectType");
            //sb.Append(",");

            ////I think these should be the columns for permission so that user can filter
            //sb.Append("$ObjectOwner");
            //sb.Append(",");
            //sb.Append("$ObjectOwnerGroup");
            //sb.Append(",");
            //sb.Append("$ObjectPublicAccess");
            //sb.Append(",");
            //sb.Append("$ObjectAssignedAccess");
            //sb.Append(",");

            CSVSave("$ObjectID", 0,0);
            CSVSave("$ObjectName", 1, 0);
            CSVSave("$ObjectParent", 2, 0);
            CSVSave("$ObjectPath", 3, 0);
            CSVSave("$ObjectType", 4, 0);
            CSVSave("$ObjectOwner", 5, 0);
            CSVSave("$ObjectOwnerGroup", 6, 0);
            CSVSave("$ObjectPublicAccess", 7, 0);
            CSVSave("$ObjectAssignedAccess", 8, 0);

        }
        public static void CSVSave(string ColumnRowValue, int ColumnNo = 0, int RowNo = 0)
        {
            //Now try to search Column No based on column Name and Row No based on Rowname[Data ID]

            //Null excpetion error occuring so, pass empty string whenever there is "null"

            if (string.IsNullOrEmpty(ColumnRowValue))
            {
                ColumnRowValue = "";
                csv[ColumnNo, RowNo] = ColumnRowValue;
            }
            else
            {
                csv[ColumnNo, RowNo] = ColumnRowValue;
            }


        }
        public static bool CSVXport(string nodeName, string csvXportPath, long nodeObjectId, StringBuilder permissionsValue)
        {
            bool returnValue = false;
            try
            {
                if (nodeName == null)
                {
                    nodeName = "EmptyCategory";
                }

                //string filePath = CSVXportPath + "\\" + NodeName + "_AttributeFields" + ".csv";
                string filePath = csvXportPath + "\\" + nodeName + "_#" + nodeObjectId.ToString() + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".csv";

                //if (General.exportFlag)
                //{
                    //csv.Save(new StreamWriter(Application.StartupPath + "\\AttributeFields_" + NodeName + ".csv"));
                    csv.Save(new StreamWriter(filePath));

                    returnValue = true;
                //}

                return returnValue;
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                return returnValue;
            }
            finally
            {

                csv.Clear();
            }
        }  

        //Passing ColumnName and Getting Column No:
        public static int GetColumnNo(string ColumnNameKey, Dictionary<string, int> ColumnNameWithNo)
        {
            int ColumNo = 0;

            foreach (var item in ColumnNameWithNo)
            {
                if (item.Key.ToString() == ColumnNameKey)
                {
                    ColumNo = item.Value;
                    break;
                }


            }


            return ColumNo;
        }

        //Saving Column[Attribute] Names:
        public static string ColumnNameBuilder(string CatName, string AttrName, long AtttributeIds, string AttributeTypes, bool AttrRequired)
        {
            string ModifiedColumnName = null;

            if (AttrRequired)
            {
                ModifiedColumnName = "$" + CatName + " : " + AttrName + " | Type : " + AttributeTypes + " | Required : " + AttrRequired;

            }
            else
            {   //Dont add Req bool value:

                ModifiedColumnName = "$" + CatName + " : " + AttrName + " | Type : " + AttributeTypes;

            }

            return ModifiedColumnName;

        }
    }
}
