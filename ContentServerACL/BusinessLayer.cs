﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Reflection;
using CISModelGenerator.CWS;
using ClosedXML.Excel;
using System.ServiceModel;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;

namespace CISModelGenerator
{
  public class BusinessLayer
    {
       

        //Create Document Management Client service
        public static DocumentManagementClient docMan = null;

        //Creating OTAuth  object and setting the Authentication token:
        public static OTAuthentication otAuth = new OTAuthentication();

        private static readonly log4net.ILog log =
       log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public DataSet CheckFileType(System.IO.FileInfo FileInf)
        {
            try
            {
                DataSet returnDataset = null;

                string FileExtension = null;

                FileExtension = FileInf.Extension;
                Console.WriteLine(FileExtension);

                if (FileExtension == ".xls")
                {
                    returnDataset = ImportExcel(FileInf.FullName, true);
                    //lobj_dataset = ReadExcelIntoDataset_xls(FileInf.FullName);
                    // If 97 - 2003 Excel version
                }
                else if (FileExtension == ".xlsx")
                {
                    returnDataset = ImportExcel(FileInf.FullName, true);
                    //lobj_dataset = ReadExcelIntoDataset_XLSX(FileInf.FullName);
                    // If 2007 to 2013Excel version
                }


                return returnDataset;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

            }

            return null;
        }

        //Old way of reading excel | this requires pre-requsites to install on client machine
        public static DataSet ImportExcel(string FileName, bool hasHeaders)
        {
            string HDR = hasHeaders ? "Yes" : "No";
            string strConn;
            if (FileName.Substring(FileName.LastIndexOf('.')).ToLower() == ".xlsx")
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=0\"";
            else
                strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + FileName + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=0\"";

            DataSet output = new DataSet();

            using (OleDbConnection conn = new OleDbConnection(strConn))
            {
                conn.Open();

                DataTable schemaTable = conn.GetOleDbSchemaTable(
                    OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                foreach (DataRow schemaRow in schemaTable.Rows)
                {
                    string sheet = schemaRow["TABLE_NAME"].ToString();
                    System.Diagnostics.Debug.WriteLine("Sheet Name : " + sheet);

                    if (!sheet.EndsWith("_"))
                    {
                        try
                        {
                            OleDbCommand cmd = new OleDbCommand("SELECT * FROM [" + sheet + "]", conn);
                            cmd.CommandType = CommandType.Text;

                            DataTable outputTable = new DataTable(sheet);
                            output.Tables.Add(outputTable);
                            new OleDbDataAdapter(cmd).Fill(outputTable);

                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);

                            log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                            log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                        }
                    }
                }
            }
            return output;
        }

        //Get the list of Sheets
        public static List<string> GetListOfSheets(string filePath)
        {
            List<string> sheetList = null;
            try
            {
                using (XLWorkbook workBook = new XLWorkbook(filePath))
                {
                    sheetList = new List<string>();

                    //Read the first Sheet from Excel file.
                    //IXLWorksheet workSheet = workBook.Worksheets;
                    foreach (IXLWorksheet workSheet in workBook.Worksheets)
                    {
                        Console.WriteLine("Worksheets: " + workSheet.Name.ToString());

                        sheetList.Add(workSheet.Name.ToString());
                    }
                                        
                   
                }

                return sheetList;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                return sheetList;
            }
        }

        //Fast and easy, no pre-requisites You can read, create excel
        public static DataTable ImportExceltoDatatable(string filePath, int workSheetIndex)
        {
            // Open the Excel file using ClosedXML.
            // Keep in mind the Excel file cannot be open when trying to read it
            using (XLWorkbook workBook = new XLWorkbook(filePath))
            {
                
                //Read the first Sheet from Excel file.
                //IXLWorksheet workSheet = workBook.Worksheet(1);
                IXLWorksheet workSheet = workBook.Worksheet(workSheetIndex);

                //Create a new DataTable.
                DataTable dt = new DataTable();

                try
                {
                    //Loop through the Worksheet rows.
                    bool firstRow = true;
                    foreach (IXLRow row in workSheet.Rows())
                    {
                        //Use the first row to add columns to DataTable.
                        if (firstRow)
                        {
                            foreach (IXLCell cell in row.Cells())
                            {
                                dt.Columns.Add(cell.Value.ToString());

                                Console.WriteLine("FirstRow Cell Value: " + cell.Value.ToString());
                            }
                            firstRow = false;
                        }
                        else
                        {
                            //Add rows to DataTable.
                            dt.Rows.Add();
                            int i = 0;

                            foreach (IXLCell cell in row.Cells(row.FirstCellUsed().Address.ColumnNumber, row.LastCellUsed().Address.ColumnNumber))
                            {                                
                                Console.WriteLine("Cell Value: " + cell.Value.ToString());

                                dt.Rows[dt.Rows.Count - 1][i] = cell.Value.ToString();
                                i++;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);

                    log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                    log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                }
               

                return dt;
            }
        }

        //Old excel format
        public static DataTable GetDataFromExcel(string filePath, int workSheetIndex)
        {            
            using (var workBook = new XLWorkbook(filePath))
            {
                var workSheet = workBook.Worksheet(workSheetIndex + 1);
                var firstRowUsed = workSheet.FirstRowUsed();
                var firstPossibleAddress = workSheet.Row(firstRowUsed.RowNumber()).FirstCell().Address;
                var lastPossibleAddress = workSheet.LastCellUsed().Address;

                // Get a range with the remainder of the worksheet data (the range used)
                var range = workSheet.Range(firstPossibleAddress, lastPossibleAddress).AsRange(); //.RangeUsed();
                                                                                                  // Treat the range as a table (to be able to use the column names)
                var table = range.AsTable();

                //Specify what are all the Columns you need to get from Excel
                var dataList = new List<string[]>
                 {

                     table.DataRange.Rows()
                         .Select(tableRow => tableRow.Field("Level One ").GetString())
                         .ToArray(),
                     table.DataRange.Rows()
                         .Select(tableRow => tableRow.Field("Level Two").GetString())
                         .ToArray(),
                     table.DataRange.Rows()
                         .Select(tableRow => tableRow.Field("Level Three").GetString())
                         .ToArray(),
                     table.DataRange.Rows()
                     .Select(tableRow => tableRow.Field("Level Four").GetString())
                     .ToArray()
                 };
                //Convert List to DataTable
                var dataTable = ConvertListToDataTable(dataList);
                        
                Console.WriteLine("Total number of rows in Excel : " + dataTable.Rows.Count);

                return dataTable;
            }
        }
        private static DataTable ConvertListToDataTable(IReadOnlyList<string[]> list)
        {
            var table = new DataTable("CustomTable");
            var rows = list.Select(array => array.Length).Concat(new[] { 0 }).Max();

            table.Columns.Add("Level One");
            table.Columns.Add("Level Two");
            table.Columns.Add("Level Three");
            table.Columns.Add("Level Four");

            for (var j = 0; j < rows; j++)
            {
                var row = table.NewRow();
                row["Level One"] = list[0][j];
                row["Level Two"] = list[1][j];
                row["Level Three"] = list[2][j];
                row["Level Four"] = list[3][j];
                table.Rows.Add(row);
            }
            return table;
        }
        
        //OT excel format
        public static DataTable GetDataFromExcelOTFormat(string filePath, int workSheetIndex)
        {
            try
            {
                using (var workBook = new XLWorkbook(filePath))
                {
                    var workSheet = workBook.Worksheet(workSheetIndex + 1);
                    var firstRowUsed = workSheet.FirstRowUsed();
                    var firstPossibleAddress = workSheet.Row(firstRowUsed.RowNumber()).FirstCell().Address;
                    var lastPossibleAddress = workSheet.LastCellUsed().Address;

                    // Get a range with the remainder of the worksheet data (the range used)
                    var range = workSheet.Range(firstPossibleAddress, lastPossibleAddress).AsRange(); //.RangeUsed();
                                                                                                      // Treat the range as a table (to be able to use the column names)
                    var table = range.AsTable();

                    var dataList = new List<string[]> { };
                    List<string> dataListByColumnNames = new List<string> { };

                    if (General.gstrExcelColumnRange == null)
                    {
                        return null;
                    }
                    //Folder level range
                    foreach (var item in table.Range(General.gstrExcelColumnRange).Cells())
                    {
                        Console.WriteLine("Column Name: " + item.Value);
                        dataListByColumnNames.Add(item.Value.ToString());
                        //Specify what are all the Columns you need to get from Excel
                        dataList.Add(table.DataRange.Rows()
                             .Select(tableRow => tableRow.Field(item.Value.ToString()).GetString())
                             .ToArray());

                    }

                    //Convert List to DataTable
                    var dataTable = ConvertListToDataTableDyn(dataList, dataListByColumnNames);
                    Console.WriteLine("Total number of rows in Excel : " + dataTable.Rows.Count);

                    return dataTable;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                return null;
            }
            
        }
        private static DataTable ConvertListToDataTableDyn(IReadOnlyList<string[]> list, List<string> columns)
        {
            try
            {
                var table = new DataTable("CustomTable");
                var rows = list.Select(array => array.Length).Concat(new[] { 0 }).Max();

                //Adding columns
                for (int eachColumn = 0; eachColumn < columns.Count; eachColumn++)
                {
                    table.Columns.Add(columns[eachColumn].ToString());

                }

                //Adding total rows to each column
                for (var eachRow = 0; eachRow < rows; eachRow++)
                {
                    var row = table.NewRow();
                    for (int eachColumn = 0; eachColumn < columns.Count; eachColumn++)
                    {
                        row[columns[eachColumn].ToString()] = list[eachColumn][eachRow];

                    }

                    table.Rows.Add(row);
                }

                return table;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                return null;
            }
            
        }
        public string returnAlphaPart(string input)
        {           
            try
            {
                //string pattern = "[FG]";
                //string replacement = "";

                //Regex regEx = new Regex(pattern);
                //string sanitized = Regex.Replace(regEx.Replace(input, replacement), @"\s+", " ");
                         
                Regex re = new Regex(@"([a-zA-Z]+)(\d+)");
                Match result = re.Match(input);

                string alphaPart = result.Groups[1].Value;
                string numberPart = result.Groups[2].Value;

                return alphaPart;
                     
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
            }
            return null ;
        }
        public int returnNumberPart(string input)
        {
            int output = 0;
            try
            {
                Regex re = new Regex(@"([a-zA-Z]+)(\d+)");
                Match result = re.Match(input);

                string alphaPart = result.Groups[1].Value;
                string numberPart = result.Groups[2].Value;

                if (Int32.TryParse(numberPart, out output))
                {
                    return output;
                }
                                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
            }
            return output;
        }

        public List<KeyValuePair<string, string>> GetGroupAndPermissioninfo(DataTable excelData, int rowValue, string groupColumnIdentifier, string folderName)
        {

            var groupList = new List<KeyValuePair<string, string>>();

            try
            {

                for (int iEachGroupColumn = 0; iEachGroupColumn < excelData.Columns.Count; iEachGroupColumn++)
                {
                    string groupNameHeader = excelData.Columns[iEachGroupColumn].ToString().Trim();
                    if (returnAlphaPart(groupNameHeader) == groupColumnIdentifier)
                    {
                        string groupName = excelData.Rows[0][groupNameHeader].ToString().Trim();

                        string rowVal = excelData.Rows[rowValue][groupNameHeader].ToString().Trim();

                        Console.WriteLine("folderName:" + folderName);
                        Console.WriteLine("groupNameHeader:" + groupNameHeader);
                        Console.WriteLine("groupName:" + groupName);
                        Console.WriteLine("permission:" + rowVal);

                        if ((!string.IsNullOrEmpty(rowVal) || !string.IsNullOrWhiteSpace(rowVal)))
                        {
                            if (rowVal != "NA")
                            {
                                groupList.Add(new KeyValuePair<string, string>(groupName, rowVal));

                            }
                        }
                       

                    }
                }
                return groupList;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                return groupList;
            }
        }
        
        //Content Server Functions:
        //Create Folder
        public long CreateFolder(long parentFolderID, string folderName, [Optional] List<KeyValuePair<string, string>> groupList)
        {   //Create Document Management Client service
            docMan = new DocumentManagementClient();

            long returnId = 0;
            log.Debug("------------------------------------------------------------------------");
            log.Debug(DateTime.Now + " | " + "Creating Folder and Attaching groups and permissions for :" + folderName );
            try
            {
                otAuth.AuthenticationToken = General.authToken;

                //Check if exists
                Node returnExistingNode = docMan.GetNodeByName(ref otAuth, parentFolderID, folderName.TrimStart().TrimEnd());

                if (returnExistingNode != null) //Existing Node
                {
                    Console.WriteLine("Existing Node ID: " + returnExistingNode.ID.ToString());
                    //return returnExistingNode.ID;
                    returnId = returnExistingNode.ID;

                    General.folderExistingCount = General.folderExistingCount + 1;
                    log.Debug(DateTime.Now + " | " + "Existing Folder| Folder ID : " + returnId);

                }
                else
                {
                    //Create Folder
                    Node returnCreatedNodeId = docMan.CreateFolder(ref otAuth, parentFolderID, folderName.TrimStart().TrimEnd(), null, null);

                    if (returnCreatedNodeId != null)
                    {
                        Console.WriteLine("Newly Created Node ID: " + returnCreatedNodeId.ID.ToString());
                        //return returnCreatedNodeId.ID;
                        returnId = returnCreatedNodeId.ID;

                        General.folderCreatedCount = General.folderCreatedCount + 1;
                        log.Debug(DateTime.Now + " | " + "Folder Created| Folder ID : " + returnId);
                        
                    }
                }

                //If add group is true
                if (General.gstrAddGroupsFlag)
                {
                    //Remove Existing Permissions
                    if (RemoveExistingPermission(returnId))
                    {
                        string ownerPermissionAndRightType = null;
                        bool ownerFlag = false;
                        foreach (var group in groupList)
                        {
                            long rightId = 0;
                           
                            //If owner column
                            if (Regex.Replace(group.Key.ToString(), @"\s+", "").ToLower() == Regex.Replace("Owner", @"\s+", "").ToLower())
                            {
                                string ownerName = group.Value.ToString();
                                char excelDelimiter = char.Parse(General.gstrExcelDelimiter);
                               
                                if (ownerName.Contains(excelDelimiter))
                                {
                                    ownerName = Utils.SplitAndGetvalue(group.Value, null, "1");
                                    ownerPermissionAndRightType = Utils.SplitAndGetvalue(group.Value, null, "0");
                                    ownerPermissionAndRightType = ownerPermissionAndRightType + ":Owner";
                                }
                                else
                                {
                                    ownerPermissionAndRightType = "FA:Owner";
                                }

                                rightId = GetOwnerMember(ownerName);                                
                                ownerFlag = true;

                            }
                            //If Public access column
                            else if (Regex.Replace(group.Key.ToString(), @"\s+", "").ToLower() == Regex.Replace("Public Access", @"\s+", "").ToLower())
                            {
                                rightId = -1;

                                log.Debug(DateTime.Now + " | " + "Adding Public Access");
                            }
                            else
                            {   //Create Group
                                rightId = SetGetGroup(group.Key);                          
                            }

                            if (rightId != 0)
                            {
                                if (ownerFlag)
                                {
                                    if (AddPermission(returnId, rightId, ownerPermissionAndRightType))
                                    {
                                        ownerFlag = false;
                                        log.Debug(DateTime.Now + " | " + "Permission is added/updated| Folder ID: " + returnId.ToString() + " | Group Right ID: " + group.Key + " | Permission Type: " + group.Value);
                                        Console.WriteLine("Permission is added | Folder ID: " + returnId.ToString() + " | Group: " + group.Key + " | Permission Type: " + group.Value);

                                    }
                                }
                                else
                                {
                                    //Add Permission
                                    if (AddPermission(returnId, rightId, group.Value))
                                    {
                                        log.Debug(DateTime.Now + " | " + "Permission is added/updated| Folder ID: " + returnId.ToString() + " | Group Right ID: " + group.Key + " | Permission Type: " + group.Value);
                                        Console.WriteLine("Permission is added | Folder ID: " + returnId.ToString() + " | Group: " + group.Key + " | Permission Type: " + group.Value);

                                    }
                                }
                                
                            }
                        }
                    }
                    
                }
                log.Debug("------------------------------------------------------------------------");
                return returnId;
            }
            catch (FaultException ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                return returnId;
            }
            finally
            {
                docMan.Close();

                ////CWS variables:
                //otAuth = null;             
            }
        }

        //Create Groups:
        public long SetGetGroup(string groupName)
        {   
            MemberServiceClient membersClient = new MemberServiceClient();
                       
            try
            {
                otAuth.AuthenticationToken = General.authToken;              

                //Check if exists
                CWS.Group returnExistingGroupRightID = membersClient.GetGroupByName(ref otAuth, groupName.TrimStart().TrimEnd());
               
                if (returnExistingGroupRightID != null) //Existing Group
                {
                    Console.WriteLine("Existing Group ID: " + returnExistingGroupRightID.ID.ToString());
                    log.Debug(DateTime.Now + " | " + "Existing Group| Group Right ID : " + returnExistingGroupRightID.ID.ToString());
                    
                    return returnExistingGroupRightID.ID;

                }
                else
                {
                    //Create Group
                    long returnGroupRightID = membersClient.CreateGroup(ref otAuth, groupName.TrimStart().TrimEnd(), null);

                    if (returnGroupRightID != 0)
                    {
                        Console.WriteLine("Newly Created Group ID: " + returnGroupRightID.ToString());
                        log.Debug(DateTime.Now + " | " + "Group Created| Group Right ID : " + returnGroupRightID.ToString());
                        
                        return returnGroupRightID;
                    }
                }

                return 0;
            }
            catch (FaultException ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                return 0;
            }
            finally
            {
                membersClient.Close();

                ////CWS variables:
                //otAuth = null;
            }
        }

        //Gets the existing user, if user doesn't exist it wont create because we dont have user creation info:
        public long GetOwnerMember(string userLoginName)
        {            
            MemberServiceClient membersClient = new MemberServiceClient();
            CWS.User returnExistingUserRightID = null;
            try
            {
                otAuth.AuthenticationToken = General.authToken;
                              
                //Check if user exists
                returnExistingUserRightID = membersClient.GetUserByLoginName(ref otAuth, userLoginName.TrimStart().TrimEnd());

                if (returnExistingUserRightID != null) //Existing User
                {
                    Console.WriteLine("User Exists, User Right ID: " + returnExistingUserRightID.ID.ToString());
                    log.Debug(DateTime.Now + " | " + "Existing User| User Right ID : " + returnExistingUserRightID.ID.ToString());

                    return returnExistingUserRightID.ID;

                }
                else
                {
                    //Take logged in user as an owner
                    returnExistingUserRightID = membersClient.GetUserByLoginName(ref otAuth, General.username.TrimStart().TrimEnd());

                    if (returnExistingUserRightID != null)
                    {
                        Console.WriteLine("Owner info is blank, taking logged in user as an owner: " + returnExistingUserRightID.ID.ToString());
                        log.Debug(DateTime.Now + " | " + "Owner info is blank, taking logged in user as an owner| User Right ID : " + returnExistingUserRightID.ID.ToString());

                        return returnExistingUserRightID.ID;
                    }
                }

                return 0;
            }
            catch (FaultException ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                return 0;
            }
            finally
            {
                membersClient.Close();

                ////CWS variables:
                //otAuth = null;
            }
        }
        //Add Permission
        public bool RemoveExistingPermission(long parentFolderID)
        {
            bool returnValue = false;
            try
            {
                otAuth.AuthenticationToken = General.authToken;

                NodeRight existingFolderNodeRight = new NodeRight();

                NodeRights existingFolderNodeRights = docMan.GetNodeRights(ref otAuth, parentFolderID);

                if (existingFolderNodeRights != null)
                {
                    //Remove Default Access
                    if (existingFolderNodeRights.OwnerGroupRight != null)
                    {
                        docMan.RemoveNodeRight(ref otAuth, parentFolderID, existingFolderNodeRights.OwnerGroupRight);
                    }
                    if (existingFolderNodeRights.OwnerRight != null)
                    {
                        docMan.RemoveNodeRight(ref otAuth, parentFolderID, existingFolderNodeRights.OwnerRight);
                    }
                    if (existingFolderNodeRights.PublicRight != null)
                    {
                        docMan.RemoveNodeRight(ref otAuth, parentFolderID, existingFolderNodeRights.PublicRight);
                    }

                    //Remove ACL rights (Assigned Access)
                    if (existingFolderNodeRights.ACLRights != null)
                    {                        
                        for (int iExistingRights = 0; iExistingRights < existingFolderNodeRights.ACLRights.Length; iExistingRights++)
                        {
                            existingFolderNodeRight = existingFolderNodeRights.ACLRights[iExistingRights];

                            docMan.RemoveNodeRight(ref otAuth, parentFolderID, existingFolderNodeRight);

                        }
                    }

                    returnValue = true;
                }

                log.Debug(DateTime.Now + " | " + "Existing Groups and Permissions are removed on : " + parentFolderID.ToString());

                return returnValue;
            }
            catch (FaultException ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                return returnValue;
            }
        }

        //Add Permission
        public bool AddPermission(long parentFolderID, long rightID, string permission)
        {          
            try
            {
                otAuth.AuthenticationToken = General.authToken;

                ////Get Permission on existing folder:
                ////NodePermissions existingFolderPermissions = new NodePermissions();
                //NodeRight existingFolderNodeRight = new NodeRight();

                //NodeRights existingFolderNodeRights = docMan.GetNodeRights(ref otAuth, parentFolderID);

                //if (existingFolderNodeRights.ACLRights != null)
                //{
                //    for (int iExistingRights = 0; iExistingRights < existingFolderNodeRights.ACLRights.Length; iExistingRights++)
                //    {
                //        if (groupRightID == existingFolderNodeRights.ACLRights[iExistingRights].RightID)
                //        {
                //            //existingFolderPermissions = existingFolderNodeRights.ACLRights[iExistingRights].Permissions;
                //            existingFolderNodeRight = existingFolderNodeRights.ACLRights[iExistingRights];
                //            if (existingFolderNodeRight != null)
                //            {
                //                //Remove permission on existing folder.
                //                //docMan.RemoveNodeRight(ref otAuth, parentFolderID, existingFolderNodeRight);

                //                //Update Permission on existing folder. | 
                //                NodePermissions updateFolderPermissions = MapPermission(permission);

                //                NodeRight updateFolderNodeRight = new NodeRight();
                //                updateFolderNodeRight.RightID = groupRightID;
                //                updateFolderNodeRight.Permissions = updateFolderPermissions;
                //                updateFolderNodeRight.Type = "ACL";

                //                docMan.UpdateNodeRight(ref otAuth, parentFolderID, updateFolderNodeRight);
                //            }
                //            //break;
                //            return true;
                //        }

                //    }
                //}

            
                string rightType = null;
                char excelDelimiter = char.Parse(General.gstrExcelDelimiter);
                       
                if (rightID == -1)
                {
                    rightType = "Public";
                    if (permission.Contains(excelDelimiter))
                    {
                        permission = Utils.SplitAndGetvalue(permission, null, "0");
                    }
                        
                }
                else
                {
                    if (permission.Contains(excelDelimiter))
                    {
                        var splittedValue = permission.Split(excelDelimiter);
                        permission = splittedValue.First().ToString();
                        rightType = splittedValue.Last().ToString();

                        if (rightType == "Owner")
                        {
                            rightType = "Owner";
                        }
                        else if(rightType == General.gstrNodeRightTypeDefaultAccess)
                        {
                            rightType = "OwnerGroup";
                        }
                        else if (rightType == General.gstrNodeRightTypeAssignedAccess)
                        {
                            rightType = "ACL";
                        }

                    }
                    else
                    {
                        rightType = "ACL";
                    }
                }

                //Add Permission
                NodePermissions addfolderPermissions = MapPermission(permission);
                NodeRight addFolderNodeRight = new NodeRight();

                if (rightType == "Owner")
                {
                    //Check if folder already has any owner group applied, then add it to ACL group
                    NodeRights existingOwnerRights = docMan.GetNodeRights(ref otAuth, parentFolderID);
                    if (existingOwnerRights.OwnerRight == null)
                    {
                        addFolderNodeRight.RightID = rightID;
                        addFolderNodeRight.Permissions = addfolderPermissions;
                        addFolderNodeRight.Type = rightType;
                    }
                    else
                    {
                        log.Debug("This folder already has Owner | Folder ID: " + parentFolderID + " | Can't add another owner, owner ID" + rightID);

                    }
                }
                else if (rightType == "OwnerGroup")
                {
                    //Check if folder already has any owner group applied, then add it to ACL group
                    NodeRights existingOwnerGroupRights = docMan.GetNodeRights(ref otAuth, parentFolderID);
                    if (existingOwnerGroupRights.OwnerGroupRight == null)
                    {
                        addFolderNodeRight.RightID = rightID;
                        addFolderNodeRight.Permissions = addfolderPermissions;
                        addFolderNodeRight.Type = rightType;
                    }
                    else
                    {
                        addFolderNodeRight.RightID = rightID;
                        addFolderNodeRight.Permissions = addfolderPermissions;
                        addFolderNodeRight.Type = "ACL";

                        log.Debug("This folder already has OwnerGroup(Default Group) | Folder ID: " + parentFolderID + " | Can't add another Owner Group, so added this group under Assigned Group, Group Right ID" + rightID);

                    }

                }
                else
                {
                    addFolderNodeRight.RightID = rightID;
                    addFolderNodeRight.Permissions = addfolderPermissions;
                    addFolderNodeRight.Type = rightType;
                }

                //Add Groups only if it is the permission list(permissio mapping json file)
                if (General.listOfPermisisonTypes.Contains(permission))
                {
                    docMan.AddNodeRight(ref otAuth, parentFolderID, addFolderNodeRight);
                }
                else
                {
                    log.Debug("Warning: Premission is not in the permission mapping json file | Permission : " + permission);

                }

                return true;
            }
            catch (FaultException ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                return false;
            }
            finally
            {
                ////CWS variables:
                //otAuth = null;
            }
        }

        //Map Permission:
        public NodePermissions MapPermission(string permission)
        {
            NodePermissions returnPermissions = new NodePermissions();         
            try
            {
                //Old Method
                //if (permission.TrimStart().TrimEnd() == "NA")
                //{
                //    returnPermissions.SeePermission = false;                   
                //}
                //else if (permission.TrimStart().TrimEnd() == "FA")
                //{
                //    returnPermissions.EditPermissionsPermission = true;
                //}
                //else if (permission.TrimStart().TrimEnd() == "FW")
                //{
                //    returnPermissions.SeePermission = true;
                //    returnPermissions.SeeContentsPermission = true;
                //    returnPermissions.ModifyPermission = true;
                //    returnPermissions.EditAttributesPermission = true;
                //    returnPermissions.AddItemsPermission = true;
                //    returnPermissions.ReservePermission = true;
                //    //There is no "Add Major Version"            AddMajorVersion
                //    //returnPermissions.AddMajorVersionPermission = true;
                //    returnPermissions.DeleteVersionsPermission = true;
                //    returnPermissions.DeletePermission = true;
                //}
                //else if (permission.TrimStart().TrimEnd() == "DA")
                //{
                //    returnPermissions.SeePermission = true;
                //    returnPermissions.SeeContentsPermission = true;
                //    returnPermissions.ModifyPermission = true;
                //    returnPermissions.EditAttributesPermission = true;
                //    returnPermissions.AddItemsPermission = true;
                //    returnPermissions.ReservePermission = true;
                //}
                //else if (permission.TrimStart().TrimEnd() == "RO")
                //{
                //    returnPermissions.SeePermission = true;
                //    returnPermissions.SeeContentsPermission = true;
                //}

                //New method -dynamic
                if (General.gstrPermissionMappings != null)
                {                    
                    foreach (var permissionItem in General.gstrPermissionMappings)
                    {
                        Console.WriteLine("{0}", permissionItem);
                        if (((Newtonsoft.Json.Linq.JProperty)permissionItem).Name == permission)
                        {
                            foreach (var permissionGroup in permissionItem)
                            {
                                Console.WriteLine("{0}", permissionGroup);

                                foreach (var eachPermission in permissionGroup.permissions)
                                {
                                    string permissionBit = Convert.ToString(eachPermission);

                                    Console.WriteLine("{0}", eachPermission);

                                    if (permissionBit.TrimStart().TrimEnd() == "see")
                                    {
                                        returnPermissions.SeePermission = true;
                                    }
                                    else if (permissionBit.TrimStart().TrimEnd() == "see_contents")
                                    {
                                        returnPermissions.SeeContentsPermission = true;
                                    }
                                    else if (permissionBit.TrimStart().TrimEnd() == "modify")
                                    {
                                        returnPermissions.ModifyPermission = true;
                                    }
                                    else if (permissionBit.TrimStart().TrimEnd() == "edit_attributes")
                                    {
                                        returnPermissions.EditAttributesPermission = true;
                                    }
                                    else if (permissionBit.TrimStart().TrimEnd() == "add_items")
                                    {
                                        returnPermissions.AddItemsPermission = true;
                                    }
                                    else if (permissionBit.TrimStart().TrimEnd() == "reserve")
                                    {
                                        returnPermissions.ReservePermission = true;
                                    }
                                    else if (permissionBit.TrimStart().TrimEnd() == "add_major_version")
                                    {
                                        //No add major version in cws
                                        //use rest api
                                    }
                                    else if (permissionBit.TrimStart().TrimEnd() == "delete_versions")
                                    {
                                        returnPermissions.DeleteVersionsPermission = true;
                                    }
                                    else if (permissionBit.TrimStart().TrimEnd() == "delete")
                                    {
                                        returnPermissions.DeletePermission = true;
                                    }
                                    else if (permissionBit.TrimStart().TrimEnd() == "edit_permissions")
                                    {
                                        returnPermissions.EditPermissionsPermission = true;
                                    }
                                }
                            }

                            break;
                        }
                        
                    }
                }
                

                return returnPermissions;
            }
            catch (FaultException ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                return returnPermissions;
            }
            finally
            {
                
            }
        }

        
        //Update Authentication
        public static void UpdateAuthenication()
        {
            //Create the Authentication Service Client:
            AuthenticationClient AuthClient = new AuthenticationClient();
            try
            {
                General.authToken = AuthClient.AuthenticateUser(General.username, General.password);

            }
            catch (FaultException ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
            }
        }

        //Authenticating User
        public void authenticateUser(string username, string password, string url)
        {
            General.username = username;
            General.password = password;

            AuthenticationClient AuthClient = null;
            try
            {
                //Create the Authentication Service Client:
                AuthClient = new AuthenticationClient();

            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                return;
            }

            try
            {        
                Console.Write("Authenticating User ...");

                try
                {
                    General.authToken = AuthClient.AuthenticateUser(General.username, General.password);

                    System.Diagnostics.Debug.WriteLine("Success!\n");
 
                    log.Info(DateTime.Now + " | " + "Authorized");
                    log.Info(DateTime.Now + " | " + "Logged in by : " + General.username);

                }
                //catch (EndpointNotFoundException ex)
                catch (Exception ex) when (ex is EndpointNotFoundException || ex is ArgumentException)
                {
                    Console.WriteLine(ex.Message);

                    log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                    log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                            
                    return;
                }

            }
            catch (FaultException ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                System.Diagnostics.Debug.WriteLine("Failed!");
                System.Diagnostics.Debug.WriteLine("{0} : {1}\n", ex.Code.Name, ex.Message);

                return;
            }
            finally
            {
                // Always close the client
                AuthClient.Close();
                System.Diagnostics.Debug.WriteLine("Client Closed!");

            }


        }

        //Creating Path Function
        public static string FullURL = null;

        public static string CreatePath(int nodeID, OTAuthentication OTAuth)
        {
          docMan = new DocumentManagementClient();
            try
            {
                Node ObjInfo = new Node();

                string StrReturnVal = null;

                string ObjName = null;
                long ParentID = 0;

                ObjInfo = docMan.GetNode(ref OTAuth, nodeID);
                ObjName = ObjInfo.Name;
                ParentID = ObjInfo.ParentID;

                //Condition: If selected folder is Enterprise or Personal WS then no need to drill
                if (ParentID != -1)
                {
                    StrReturnVal = getParent(nodeID, OTAuth);

                }


                FullURL = FullURL + ObjName;

                System.Diagnostics.Debug.WriteLine(FullURL);

                return FullURL;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);

                return null;
            }
            finally
            {
                docMan.Close();
                FullURL = null;
            }
        }

        public static string getParent(long nodeID, OTAuthentication OTAuth)
        {
            try
            {
                Node childObjInfo = new Node();
                Node EntprseWSNODE = new Node();
                Node PersonalWSNODE = new Node();

                long ParentIDChild = 0;

                string ParName = null;


                long EntWrkspc = 0;
                long PersonalWrkspc = 0;

                EntprseWSNODE = docMan.GetRootNode(ref OTAuth, "EnterpriseWS");
                EntWrkspc = EntprseWSNODE.ID;

                PersonalWSNODE = docMan.GetRootNode(ref OTAuth, "PersonalWS");
                PersonalWrkspc = PersonalWSNODE.ID;

                childObjInfo = docMan.GetNode(ref OTAuth, nodeID);
                ParentIDChild = childObjInfo.ParentID;


                if (ParentIDChild == -1)
                {
                    ParentIDChild = childObjInfo.VolumeID;

                    //If Parent is -1 then we cant get name of it, so lets take the root container name itself

                    ParName = childObjInfo.Name;
                }
                else
                {
                    childObjInfo = docMan.GetNode(ref OTAuth, ParentIDChild);
                    ParName = childObjInfo.Name;

                }

                FullURL = ParName + ":" + FullURL;

                if (EntWrkspc == ParentIDChild)
                //if (ParentID == -1)
                {
                    //FullURL = FullURL.Remove(FullURL.Length - 1)
                    System.Diagnostics.Debug.WriteLine(FullURL);

                }
                else if (PersonalWrkspc == ParentIDChild)
                {
                    System.Diagnostics.Debug.WriteLine(FullURL);
                }
                else
                {
                    if (ParentIDChild == childObjInfo.VolumeID)//(ParentID ==-2000)
                    {
                        //then it root folder skip
                    }
                    else
                    {
                       getParent(ParentIDChild, OTAuth);
                    }

                }

                return FullURL;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                log.Info(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                log.Debug(DateTime.Now + " | " + MethodBase.GetCurrentMethod().Name + " Exception : " + ex.Message + " | " + ex.StackTrace);
                return null;

            }

        }
    }
}
