﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CISModelGenerator.Model
{
    class ACL
    {
        public partial class GroupsAndPermissions
        {
            [JsonProperty("links")]
            public Links Links { get; set; }

            [JsonProperty("results")]
            public Results Results { get; set; }
        }

        public partial class Links
        {
            [JsonProperty("data")]
            public LinksData Data { get; set; }
        }

        public partial class LinksData
        {
            [JsonProperty("self")]
            public Self Self { get; set; }
        }

        public partial class Self
        {
            [JsonProperty("body")]
            public string Body { get; set; }

            [JsonProperty("content_type")]
            public string ContentType { get; set; }

            [JsonProperty("href")]
            public string Href { get; set; }

            [JsonProperty("method")]
            public string Method { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }
        }

        public partial class Results
        {
            [JsonProperty("data")]
            public ResultsData Data { get; set; }
        }

        public partial class ResultsData
        {
            [JsonProperty("permissions")]
            public Permission[] Permissions { get; set; }

            [JsonProperty("properties")]
            public Properties Properties { get; set; }
        }

        public partial class Permission
        {
            [JsonProperty("permissions")]
            public string[] Permissions { get; set; }

            [JsonProperty("right_id")]
            public long? RightId { get; set; }

            [JsonProperty("right_id_expand", NullValueHandling = NullValueHandling.Ignore)]
            public RightIdExpand RightIdExpand { get; set; }

            [JsonProperty("type")]
            public string Type { get; set; }
        }

        public partial class RightIdExpand
        {
            [JsonProperty("birth_date")]
            public object BirthDate { get; set; }

            [JsonProperty("business_email")]
            public object BusinessEmail { get; set; }

            [JsonProperty("business_fax")]
            public object BusinessFax { get; set; }

            [JsonProperty("business_phone")]
            public object BusinessPhone { get; set; }

            [JsonProperty("cell_phone")]
            public object CellPhone { get; set; }

            [JsonProperty("deleted")]
            public bool Deleted { get; set; }

            [JsonProperty("first_name")]
            public object FirstName { get; set; }

            [JsonProperty("gender")]
            public object Gender { get; set; }

            [JsonProperty("group_id", NullValueHandling = NullValueHandling.Ignore)]
            public long? GroupId { get; set; }

            [JsonProperty("home_address_1")]
            public object HomeAddress1 { get; set; }

            [JsonProperty("home_address_2")]
            public object HomeAddress2 { get; set; }

            [JsonProperty("home_fax")]
            public object HomeFax { get; set; }

            [JsonProperty("home_phone")]
            public object HomePhone { get; set; }

            [JsonProperty("id")]
            public long Id { get; set; }

            [JsonProperty("last_name")]
            public object LastName { get; set; }

            [JsonProperty("middle_name")]
            public object MiddleName { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("name_formatted")]
            public string NameFormatted { get; set; }

            [JsonProperty("office_location")]
            public object OfficeLocation { get; set; }

            [JsonProperty("pager")]
            public object Pager { get; set; }

            [JsonProperty("personal_email")]
            public object PersonalEmail { get; set; }

            [JsonProperty("personal_interests")]
            public object PersonalInterests { get; set; }

            [JsonProperty("personal_url_1")]
            public object PersonalUrl1 { get; set; }

            [JsonProperty("personal_url_2")]
            public object PersonalUrl2 { get; set; }

            [JsonProperty("personal_url_3")]
            public object PersonalUrl3 { get; set; }

            [JsonProperty("personal_website")]
            public object PersonalWebsite { get; set; }

            [JsonProperty("photo_id")]
            public object PhotoId { get; set; }

            [JsonProperty("photo_url")]
            public object PhotoUrl { get; set; }

            [JsonProperty("time_zone")]
            public object TimeZone { get; set; }

            [JsonProperty("title")]
            public object Title { get; set; }

            [JsonProperty("type")]
            public long Type { get; set; }

            [JsonProperty("type_name")]
            public string TypeName { get; set; }

            [JsonProperty("leader_id")]
            public object LeaderId { get; set; }
        }

        public partial class Properties
        {
            [JsonProperty("container")]
            public bool Container { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("type")]
            public long Type { get; set; }
        }
    }
}
