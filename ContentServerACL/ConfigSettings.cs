﻿using System;
using System.Xml;
using System.Configuration;
using System.Reflection;

namespace CISModelGenerator
{
    class ConfigSettings
    {
        private static string NodePath = "//system.serviceModel//client//endpoint";
        private ConfigSettings() { }

        public static string GetEndpointAddress()
        {
            return ConfigSettings.loadConfigDocument().SelectSingleNode(NodePath).Attributes["address"].Value;

        }

        public static void SaveEndpointAddress(string endpointAddress)
        {
            // load config document for current assembly
            XmlDocument doc = loadConfigDocument();

            // retrieve appSettings node
            //  XmlNode node = doc.SelectSingleNode(NodePath);

            //Retrieving Multi Endpoint Address
            XmlNodeList NdList = doc.SelectNodes(NodePath);

            string RetVal;

            foreach (XmlNode node in NdList)
            {
                RetVal = node.Attributes["address"].Value;

                if (node == null)
                    throw new InvalidOperationException("Error. Could not find endpoint node in config file.");

                try
                {
                    // select the 'add' element that contains the key
                    //XmlElement elem = (XmlElement)node.SelectSingleNode(string.Format("//add[@key='{0}']", key));
                    string NewEndPointAddress;
                    NewEndPointAddress = ReplacOnlyHostName(node.Attributes["address"].Value.ToString(), endpointAddress);

                    node.Attributes["address"].Value = NewEndPointAddress;

                    doc.Save(getConfigFilePath());
                }
                catch (Exception e)
                {
                    throw e;
                }
            }



        }

        public static XmlDocument loadConfigDocument()
        {
            System.Xml.XmlDocument doc = null;
            try
            {
                doc = new System.Xml.XmlDocument();
                doc.Load(getConfigFilePath());
                return doc;
            }
            catch (System.IO.FileNotFoundException e)
            {
                throw new Exception("No configuration file found.", e);
            }
        }

        private static string getConfigFilePath()
        {
            return Assembly.GetExecutingAssembly().Location + ".config";
        }

        private static string ReplacOnlyHostName(string OldHost, string NewHost)
        {
            try
            {
                string[] SplitOldVal = null;
                string[] SplitNewVal = null;

                string CWS_Service;
                string OldCWS_Service;
                string ModifiedValue;

                //Splitting and Webservice Name from Old Host Name or Default Host Name
                SplitOldVal = OldHost.Split('/');
                CWS_Service = SplitOldVal[SplitOldVal.Length - 1];
                System.Diagnostics.Debug.WriteLine("Web Service Name : " + CWS_Service);


                //Now Replacing this with new added Host URL           
                SplitNewVal = NewHost.Split('/');
                OldCWS_Service = (SplitNewVal[SplitNewVal.Length - 1]);

                ModifiedValue = NewHost.Replace(OldCWS_Service, CWS_Service);

                return ModifiedValue;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error : " + ex.Message);
                return null;
            }
        }


    }
}
